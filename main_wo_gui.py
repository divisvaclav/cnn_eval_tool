import sys
import os
import re
import copy
import time
import numpy as np
import json
from tqdm import tqdm
import collections

import scripts.GeneralFunctions as GeneralFunctions
import scripts.SafetyAlgorithm as SafetyAlgorithm

if __name__ == "__main__":

    Functions = GeneralFunctions.GeneralFunctions()
    Safety = SafetyAlgorithm.SafetyAlgorithm()

    split_data_according_to_the_class = False
    if split_data_according_to_the_class:
        validation_labels_path = '/media/vincie/1,5TB/ImageNet_validation/real.json' # /media/vincie/1,5TB/ImageNet_validation
        validation_images_path = "/media/vincie/1,5TB/ImageNet_validation/ILSVRC2012_img_val"
        split_path = "/media/vincie/1,5TB/ImageNet_validation/ILSVRC2012_img_val_classes_split"
        Safety.split_imageNet_validationSet_overClass(validation_labels_path, validation_images_path, split_path)


    """ Comment """
    def get_model_and_layers(_models_name, _number_of_layers):
        if _models_name == "VGG16":
            _list_of_layers = Safety.open_VGG16_Keras(_number_of_layers)
        elif _models_name == "ResNet50V2":
            _list_of_layers = Safety.open_ResNet50V2_Keras(_number_of_layers)
        elif _models_name == "MobileNetV2":
            _list_of_layers = Safety.open_MobileNetV2_Keras(_number_of_layers)
        elif _models_name == "EfficientNetB0":
            _list_of_layers = Safety.open_EfficientNetB0_Keras(_number_of_layers)
        else:
            print("Wrong models name")
        return _list_of_layers


    models_name_list = ["VGG16", "ResNet50V2", "MobileNetV2", "EfficientNetB0"]
    models_name_list = ["ResNet50V2"]

    """ Comment """
    tested_classes = ["mountain_bike"]

    first = os.path.join("/home", "vincie", "Projects")
    second = os.path.join("/home", "arrk", "Vaclav", "Projects")
    third = "D:\\PycharmProjects"

    path_dataset = os.path.join(third, "cnn_eval_tool", "data", "dataset", "reduced_validation_set")
    path_adversary_samples = os.path.join(third, "cnn_eval_tool", "data", "dataset", "adversary")
    path_statistics = os.path.join(third, "cnn_eval_tool", "data", "statistics")
    path_benchmark = os.path.join(third, "cnn_eval_tool", "data", "benchmark")

    """ """
    dict_of_critical_layers = { "VGG16":
                                {
                                    "mountain_bike": ["block1_conv_2", "block3_conv3", "block5_conv2"],
                                    "street_sign": ["block1_conv_2", "block2_conv2", "block5_conv1"]
                                },
                                "ResNet50V2":
                                    {
                                        "mountain_bike": ["conv2_block1_0_conv", "conv3_block2_1_conv", "conv5_block3_2_conv"],
                                        "street_sign": ["conv2_block1_0_conv", "conv3_block3_1_conv", "conv5_block2_1_conv"]
                                    },
                                "MobileNetV2":
                                    {
                                        "mountain_bike": ["block2_project", "block6_project", "block13_project"],
                                        "street_sign": ["block1_project", "block6_project", "block13_project"]
                                    },
                                "EfficientNetB0":
                                    {
                                        "mountain_bike": ["block1a_project_conv", "block4a_project_conv", "block7a_project_conv"],
                                        "street_sign": ["block2a_project_conv", "block4a_project_conv", "block7a_project_conv"]
                                    }
                                }


    """ Analysis variables """
    analyse_offline_data = True
    calculate_just_confidence = False
    allow_logging = True
    allow_plotting = False
    all_samples = True
    take_critical_layers = False
    take_adversary = False
    number_of_neurons = 100

    # read stored dictionary from json
    if analyse_offline_data:

        for all_models in models_name_list:

            list_of_layers = get_model_and_layers(all_models, "all")
            Safety.get_model_with_custom_layers(list_of_layers, _custom_model=False)
            Safety.model.trainable = False
            Safety.custom_layers_model.trainable = False

            for all_classes in tested_classes:

                if take_adversary:
                    files_path = os.path.join(path_adversary_samples, all_models, all_classes)
                    temp_path_statistics = os.path.join(path_statistics, "normal")
                    temp_path_benchmark = os.path.join(path_benchmark, "adversary")
                else:
                    files_path = os.path.join(path_dataset, all_classes)
                    temp_path_statistics = os.path.join(path_statistics, "normal")
                    temp_path_benchmark = os.path.join(path_benchmark, "normal")

                models_arch = os.path.join(temp_path_statistics, all_models + "_arch.json")
                logging_file = os.path.join(temp_path_statistics, all_models + "_" + all_classes + "_statistics.json")
                #text = None
                #if os.path.isfile(logging_file):
                #    with open(logging_file, "r") as read_file:
                #        text = read_file.read()
                #        new_text = text.replace("}{" , ", ")
                #with open("test.json", "w") as f:
                #    f.write(new_text)

                if os.path.isfile(models_arch):
                    with open(models_arch, "r") as read_file:
                        models_arch_dict = json.load(read_file)

                if os.path.isfile(logging_file):
                    with open(logging_file, "r") as read_file:
                        stat_dict = json.load(read_file)
                        conv_dict = collections.defaultdict(list)
                        proj_dict = collections.defaultdict(list)
                        #print(stat_dict)
                        for image_name in stat_dict.keys():
                            layers_dict = stat_dict[image_name][0]
                            #print(layers_dict)
                            for layers_name in layers_dict.keys():
                                temp_indices_dict = dict()
                                if "project" in layers_name:
                                    for indices_dict in layers_dict[layers_name]:
                                        proj_dict[layers_name].append(indices_dict)
                                else:
                                    for indices_dict in layers_dict[layers_name]:
                                        conv_dict[layers_name].append(indices_dict)
                                #print (layers_dict[layers_name])
                            if allow_plotting:
                                Functions.plot_CDP_results(temp_path_benchmark, proj_dict, image_name, all_models,
                                                           all_classes, Safety.dict_of_weights, Safety.criticality_tau)
                                Functions.plot_CDP_results(temp_path_benchmark, conv_dict, image_name, all_models,
                                                           all_classes, Safety.dict_of_weights, Safety.criticality_tau)

                        print("Building statistics from {} samples, for model {}, and class {}".format(len(stat_dict.keys()), all_models, all_classes ))
                        worst_neurons_dict = Functions.joy_plots(stat_dict, all_classes, all_models, temp_path_benchmark, _n_worst=number_of_neurons)
                else:
                    print("Statistics file not found: {}".format(logging_file))

                Safety.analyse_accuracy_of_masked_model(files_path, temp_path_benchmark,
                                                        all_models, all_classes, worst_neurons_dict, _n_worst=number_of_neurons)

    else:
        # iterate through all models in models_name_list
        for all_models in models_name_list:

            # iterate through all defined classes in tested_classes
            for all_classes in tested_classes:

                if take_critical_layers:
                    _ = get_model_and_layers(all_models, "all")
                    list_of_layers = dict_of_critical_layers[all_models][all_classes]
                    list_of_layers.append(Safety.model.layers[-1].name) # adding prediction layer
                else:
                    list_of_layers = get_model_and_layers(all_models, "all")
                Safety.get_model_with_custom_layers(list_of_layers, _custom_model=False)

                Safety.model.trainable = False
                Safety.custom_layers_model.trainable = False

                ori_ind = Safety.list_of_classes_labels.index(all_classes)

                # choose between adversary or evaluation images
                if take_adversary:
                    files_path = os.path.join(path_adversary_samples, all_models, all_classes)
                    temp_path_statistics = os.path.join(path_statistics, "adversary")
                    temp_path_benchmark = os.path.join(path_benchmark, "adversary")
                else:
                    files_path = os.path.join(path_dataset, all_classes)
                    temp_path_statistics = os.path.join(path_statistics, "normal")
                    temp_path_benchmark = os.path.join(path_benchmark, "normal")

                if not os.path.exists(temp_path_statistics):
                    os.makedirs(temp_path_statistics)

                models_arch_logging = os.path.join(temp_path_statistics, all_models + "_arch.json")
                dict_of_layers, dict_of_weights = Safety.get_layers_and_weights(Safety.custom_layers_model)
                with open(models_arch_logging, 'w') as fp:
                    json.dump(dict_of_layers, fp)

                if not os.path.exists(temp_path_benchmark):
                    os.makedirs(temp_path_benchmark)

                files = [x for x in os.listdir(files_path) if not x.startswith('.')]
                files.sort()
                if not all_samples:
                    files = files[:1]
                files = files[23:]

                # iterate through all images found in files_path
                for file_name in tqdm(files):
                    Safety.input_image_np, Safety.input_image  = Functions.load_image(
                            os.path.join(files_path, file_name), widht=224, height=224, normalize=False)

                    Safety.preprocess_input_for_used_model(all_models)

                    # calculates and stores confidence - removes images from dataset in case "remove" == True
                    if calculate_just_confidence:
                        desired_conf, desired_ind = Safety.get_conf_and_class(files_path, file_name, all_classes, remove=False)

                    # analysis loop
                    else:
                        Safety.clear_variables()
                        Safety.statistics_dict = collections.defaultdict(list)

                        desired_conf, desired_ind = Safety.get_conf_and_class(files_path, file_name, all_classes, remove=False)

                        """ 1) criticality calculation based on plain filter masking """
                        Safety.analyse_CDP_plain_masking(temp_path_benchmark, file_name, all_models,
                                                         desired_conf, desired_ind, all_classes, ori_ind,
                                                         take_adversary)

                        if allow_logging:
                            json_logging_path = os.path.join(temp_path_statistics, all_models + "_" + all_classes + "_statistics.json")
                            with open(json_logging_path, 'a') as fp:
                                json.dump(Safety.statistics_dict, fp)


                # for number_of_neurons in range(0,32):
                #    Safety.mask_and_get_confidence('conv1', [number_of_neurons])

                # original_ind, original_cls, original_conf = Safety.get_original_conf_and_class()
                #
                # Safety.analyse_CDP_plain_masking(path_benchmark, file_name, model_name, original_ind, original_cls, original_conf)
                #
                # for number_of_neurons in range(1,32):
                #     Safety.clear_variables()
                #     Safety.analyse_CDP_dependently_masking(path_benchmark, file_name, model_name, number_of_neurons, original_ind, original_cls, original_conf)
