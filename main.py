import sys
import os
import re
import subprocess
import cv2
from sys import platform


from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

from matplotlib.figure import Figure
import numpy as np
from numpy.random import default_rng
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 8})


from PySide6 import QtCore, QtWidgets
from PySide6.QtWidgets import QGridLayout, QStyle
from PySide6.QtCore import Qt, QPoint, QSize
from PySide6.QtGui import QImage, QPixmap
from Qt_GUI.ui_mainwindow import Ui_MainWindow

import scripts.GeneralFunctions as GeneralFunctions
import scripts.OpenFileDialog as OpenFileDialog
from scripts.VisualizeCNN import CNNCanvas
import scripts.GuiFunctions as GuiFun
import scripts.Monitor as Monitor
import scripts.DataSets as DataSet
import scripts.SafetyAlgorithm as SafetyAlgorithm


class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):

    def __init__(self, parent=None):
        super(MyApp, self).__init__(parent)
        self.setupUi(self)

        self.GenFun = GeneralFunctions.GeneralFunctions()
        self.FileDlg = OpenFileDialog.OpenFileDialog()
        self.DataSet = DataSet.DataSet()

        self.Safety = SafetyAlgorithm.SafetyAlgorithm()
        self.Safety.open_MobileNetV2_Keras(5)
        self.eval_label_opened_cnn.setText("default MobileNetV2 selected ")
        self.test_set = list()

        debugging = True
        if debugging:
            input_files = os.path.join("data", "dataset", "custom")
            self.test_set, list_of_files = self.DataSet.loadTestSetFromPath(input_files)
            GuiFun.treeWidgetAddItemsFromList(self.eval_treeWidget_testset, list_of_files)



        # create a SFML view inside the main frame
        self.SFMLView = CNNCanvas(self.eval_frame, QPoint(1, 1), QSize(1920, 1080), self.Safety.model, False, 0)

        self.SFMLView.visualization_running_signal.connect(self.PauseVisualization)
        self.SFMLView.signal_video_changed.connect(self.RefreshVideo)
        self.SFMLView.set_Safety(self.Safety)
        self.SFMLView.testSet = self.test_set
        self.SFMLView.show()

        self.eval_pushButton_visualize.setEnabled(True)
        self.eval_treeWidget_CNN.setHeaderLabel("Network's layers")
        self.eval_treeWidget_CNN.setSortingEnabled(False)
        self.eval_treeWidget_CNN.itemSelectionChanged.connect(self.SelectedLayer)
        self.eval_treeWidget_CNN.itemClicked.connect(self.CheckedLayer)
        GuiFun.treeWidgetAddItemsFromDict(self.eval_treeWidget_CNN, self.Safety.dict_of_layers)

        self.list_of_supported_layers = ["Conv2D", "DepthwiseConv2D", "Dense"]
        self.eval_treeWidget_testset.itemClicked.connect(self.CheckedImage)
        self.AllowFeatures(False)

        self.eval_pushButton_open_network.clicked.connect(self.OpenNetwork)
        self.eval_pushButton_open_testset.clicked.connect(self.OpenTestSet)

        self.eval_pushButton_refresh_cameras.setFixedHeight(24)
        self.eval_pushButton_refresh_cameras.setIconSize(QSize(16, 16))
        self.eval_pushButton_refresh_cameras.setIcon(self.style().standardIcon(QStyle.SP_BrowserReload))
        self.eval_pushButton_refresh_cameras.clicked.connect(self.RefreshCameras)
        self.eval_pushButton_visualize.clicked.connect(self.VisualizeNetwork)

        self.eval_checkBox_adding_noise.stateChanged.connect(self.AddNoise)
        self.eval_checkBox_G_channel.stateChanged.connect(self.ClearRchannel)
        self.eval_checkBox_R_channel.stateChanged.connect(self.ClearGchannel)
        self.eval_checkBox_B_channel.stateChanged.connect(self.ClearBchannel)
        self.eval_checkBox_grad_cam.stateChanged.connect(self.ApplyGradCAM)

        self.eval_tableWidget_layers_filters.setStyleSheet(
            "font: 6pt; padding: 0px; border-spacing: 2px; QTableWidget::item { font: 8pt; padding: 0px; }")
        self.eval_tableWidget_layers_filters.itemPressed.connect(self.SelectedFilter)

        self.grid_layout_filters_values = QGridLayout()
        self.eval_verticalLayout_filters.addLayout(self.grid_layout_filters_values)

        self.eval_doubleSpinBox_noise_mean.valueChanged.connect(self.NoiseChanged)
        self.eval_doubleSpinBox_noise_std.valueChanged.connect(self.NoiseChanged)

        self.eval_pushButton_applyTopology.clicked.connect(self.ApplyTopology)

        self.eval_checkBox_add_filter_noise.stateChanged.connect(self.AddFiterNoise)
        self.selected_layer = "None"
        self.selected_filter = 0

        self.eval_spinBox_number_filters.valueChanged.connect(self.FilterChanged)
        self.eval_spinBox_number_filters.setValue(5)
        self.number_of_masked_filters = 5

        self.eval_horizontalSlider_max_responses.valueChanged.connect(self.SetMaximumResponses)

        self.eval_pushButton_open_mask.clicked.connect(self.OpenCustomMask)

        self.checked_layers = list()

        # defining canvas for future histogram
        self._canvas = FigureCanvas(Figure(figsize=(4, 2)))
        self._ax = self._canvas.figure.subplots()
        self.eval_verticalLayout_filters_masking.addWidget(self._canvas)
        self.bin = 50

        self.eval_pushButton_cdp.clicked.connect(self.StartCDP)
        self.row = 0
        self.use_camera = False

    def StartCDP(self):
        # self.SFMLView.criticalDecissionPath()
        return 0

    def FilterChanged(self):
        self.number_of_masked_filters = self.eval_spinBox_number_filters.value()

    def AddFiterNoise(self):
        self.SFMLView.masked_layer = self.selected_layer
        how_many_filters = self.Safety.dict_of_weights[self.selected_layer][0].shape[3]
        rng = default_rng()
        list_of_filters = list(rng.choice(how_many_filters, size=self.number_of_masked_filters, replace=False))
        self.eval_label_random_filters.setText(str(list_of_filters))
        self.SFMLView.list_of_masked_filters = list_of_filters
        if self.eval_checkBox_add_filter_noise.checkState() == QtCore.Qt.Checked:
            self.SFMLView.D = True
        else:
            self.SFMLView.D = False

    def SetMaximumResponses(self, value):
        if value == self.eval_horizontalSlider_max_responses.maximum():
            self.SFMLView.critical_path = False
            self.eval_label_max_responses.setText("all")
        else:
            self.SFMLView.critical_path = True
            self.SFMLView.num_of_max_activations = int(value)
            self.Safety.num_of_max_activations = int(value)
            self.eval_label_max_responses.setText(str(value))

    def ApplyTopology(self):
        root = self.eval_treeWidget_CNN.invisibleRootItem()
        signal_count = root.childCount()
        self.checked_layers = list()

        for i in range(signal_count):
            item = root.child(i)
            if item.checkState(0) == QtCore.Qt.Checked:
                self.checked_layers.append(item.text(0))
        self.Safety.get_model_with_custom_layers(self.checked_layers)

        # pause the visualization
        self.SFMLView.on_update_get_outputs_pause = True

        # reinit the textures
        self.SFMLView.initTextures(0, self.checked_layers, self.row)
        geometry = self.eval_frame.geometry()

        self.SFMLView.resizeCanvas(QPoint(1, 1), QSize(geometry.width()-10, geometry.height()-10))
        self.SFMLView.on_update_get_outputs_pause = False
        self.AllowFeatures(True)
        self.eval_pushButton_visualize.setText("Pause visualization")

    def NoiseChanged(self, value):
        print(value)
        noise = self.SFMLView.setNoise(self.eval_doubleSpinBox_noise_mean.value(),
                                       self.eval_doubleSpinBox_noise_std.value())
        self.GenFun.plotWeightsHistogram(noise.ravel(), self._ax, self.bin, self._canvas)

    def RefreshVideo(self, frame):
        # After each frame beeings process values have to be shown
        self.eval_label_video_stream.setPixmap(frame)

    def AddNoise(self, state):
        self.SFMLView.N = state

    def ClearRchannel(self, state):
        self.SFMLView.R = state

    def ClearGchannel(self, state):
        self.SFMLView.G = state

    def ClearBchannel(self, state):
        self.SFMLView.B = state

    def ApplyGradCAM(self, state):
        # pause the visualization
        self.SFMLView.on_update_get_outputs_pause = True
        self.SFMLView.grad_cam = state
        # reinit the textures
        self.SFMLView.initTextures(0, self.checked_layers, self.row)
        self.SFMLView.on_update_get_outputs_pause = False

    def AllowFeatures(self, flag):
        self.eval_checkBox_adding_noise.setEnabled(flag)
        self.eval_checkBox_G_channel.setEnabled(flag)
        self.eval_checkBox_R_channel.setEnabled(flag)
        self.eval_checkBox_B_channel.setEnabled(flag)

    def SelectedFilter(self, item):
        self.selected_filter = int(item.text())
        weights = self.Safety.dict_of_weights[self.selected_layer][0]
        rows = weights.shape[0]
        cols = weights.shape[1]

        self.eval_checkBox_add_filter_noise.setEnabled(True)

        if self.grid_layout_filters_values.count() > (cols * rows):
            for i in reversed(range(self.grid_layout_filters_values.count())):
                widgetToRemove = self.grid_layout_filters_values.itemAt(i).widget()
                # remove it from the layout list
                self.grid_layout_filters_values.removeWidget(widgetToRemove)
                # remove it from the gui
                widgetToRemove.setParent(None)

        if len(weights.shape) == 4:
            if self.selected_filter > (weights.shape[3] - 1):
                channel = int(self.selected_filter / (weights.shape[3]))
                self.selected_filter = int(self.selected_filter % (weights.shape[3]))
            else:
                channel = 0
            # depth wise separable conv
            if weights.shape[0] == 1 and weights.shape[1] == 1:
                GuiFun.createSpinBox(self.grid_layout_filters_values,
                                     float(weights[:, :, channel, self.selected_filter]),
                                     0, 0)
            # conv
            else:
                for i in range(rows):
                    for j in range(cols):
                        GuiFun.createSpinBox(self.grid_layout_filters_values,
                                             float(weights[i, j, channel, self.selected_filter]),
                                             i, j)
        # dense
        elif len(weights.shape) == 2:
            if self.selected_filter > (weights.shape[1] - 1):
                channel = int(self.selected_filter / (weights.shape[1]))
                self.selected_filter = int(self.selected_filter % (weights.shape[1]))
            else:
                channel = 0
            GuiFun.createSpinBox(self.grid_layout_filters_values,
                                 float(weights[channel, self.selected_filter]),
                                 0, 0)

    def getMaxWeightValueCONV(self, weights_array, rows, columns):
        sum_array = []
        for i in range(rows):
            for j in range(columns):
                sum_array.append(np.sum(np.abs(weights_array[:, :, i, j])))
        return np.max(np.asarray(sum_array)), sum_array

    def getMaxWeightValueDENSE(self, weights_array, rows, columns):
        sum_array = []
        for i in range(rows):
            for j in range(columns):
                sum_array.append(np.sum(np.abs(weights_array[i, j])))
        return np.max(np.asarray(sum_array)), sum_array

    def CheckedImage(self, item, column):
        text = item.text(0)
        if text.split(".")[-1] in ["jpg", "png"]:
            self.row = self.eval_treeWidget_testset.indexOfTopLevelItem(item)
            widht = self.test_set[self.row].shape[0]
            height = self.test_set[self.row].shape[1]
            image = (255 * self.test_set[self.row, :, :, :3]).astype(np.uint8)
            p_image = QImage(image.data, widht, height, 3 * height, QImage.Format_RGB888)
            p = QPixmap.fromImage(p_image)
            self.RefreshVideo(p.scaled(widht, height, Qt.KeepAspectRatio))
            self.SFMLView.index_testset = self.row
            # update
        else:
            self.SFMLView.changeStream(None, text)
        print(self.row)
        return self.row
    def SelectedLayer(self):
        get_selected_list = self.eval_treeWidget_CNN.selectedItems()
        sum_array = None
        maxi = None
        index = None
        for selected_item in get_selected_list:
            if selected_item.parent() == None:
                base_node = get_selected_list[0]
                self.eval_label_active_layer.setText(base_node.text(0))
                self.selected_layer = base_node.text(0)
                # Pass the name to sfml to be able to visualize the saliency maps
                self.SFMLView.selected_layer_name = self.selected_layer
                # visualizing matrix of filters
                weights_array = self.Safety.dict_of_weights[base_node.text(0)][0]
                x = weights_array.flatten()
                # to filter only depth separable and conv layers
                if self.eval_checkBox_visulize_weights.checkState() == QtCore.Qt.Checked:
                    if len(weights_array.shape) == 4:

                        if weights_array.shape[0] == 1 and weights_array.shape[1] == 1:
                            # calculating the max response
                            maxi, sum_array = self.getMaxWeightValueCONV(weights_array,
                                                                        weights_array.shape[2], weights_array.shape[3])
                            index = 2

                        else:
                            maxi, sum_array = self.getMaxWeightValueCONV(weights_array,
                                                                        weights_array.shape[2], weights_array.shape[3])
                            index = 2

                    elif len(weights_array.shape) == 2:
                        maxi, sum_array = self.getMaxWeightValueDENSE(weights_array,
                                                                     weights_array.shape[0], weights_array.shape[1])
                        index = 0
                    if sum_array:
                        GuiFun.createTableContent(self.eval_tableWidget_layers_filters,
                                                  weights_array.shape[index], weights_array.shape[index + 1],
                                                  maxi, sum_array)

                self.GenFun.plotWeightsHistogram(x, self._ax, self.bin, self._canvas)

    def CheckedLayer(self, item, column):
        root = self.eval_treeWidget_CNN.invisibleRootItem()
        signal_count = root.childCount()
        checked_layers = list()

        for i in range(signal_count):
            item = root.child(i)

            if item.checkState(0) == QtCore.Qt.Checked:
                checked_layers.append(item.text(0).lower())

        if len(checked_layers) > 0:
            self.eval_pushButton_visualize.setEnabled(True)
            self.eval_pushButton_applyTopology.setEnabled(True)
        else:
            self.eval_pushButton_visualize.setEnabled(False)
            self.eval_pushButton_applyTopology.setEnabled(False)

    def OpenNetwork(self):
        # Open and choose file
        #temp_text = QtCore.QDateTime.currentDateTime().toString() + "You just saved file"
        input_file = self.FileDlg.openNetworkDialog()
        if input_file:
            temp_name = input_file[0].split(".")[0]
            model_path = temp_name + ".json"
            weights_path = temp_name + ".h5"

            self.Safety.model = self.GenFun.open_custom_model(model_path, weights_path, _remove_diag=False,
                                                              _add_testing_layer=False)

            self.eval_label_opened_cnn.setText(temp_name)

            self.SFMLView.setModel(self.Safety.model)

            self.eval_pushButton_visualize.setEnabled(True)

    def OpenCustomMask(self, load_numpy_array=None):
        # Open and choose numpy array
        input_file = self.FileDlg.openCustomMaskDialog()
        if input_file:
            self.eval_checkBox_add_custom_mask.setEnabled(True)
            self.custom_mask_numpy = load_numpy_array(input_file, newsize=(224, 224))

    def OpenTestSet(self):
        input_files = self.FileDlg.openTestSetDialog()
        if input_files:
            if len(input_files) > 0:
                self.test_set = self.DataSet.loadTestSet(input_files)
                GuiFun.treeWidgetAddItemsFromList(self.eval_treeWidget_testset, input_files)
                self.SFMLView.testSet = self.test_set
            else:
                GuiFun.treeWidgetAddItemsFromList(self.eval_treeWidget_testset, input_files)
                self.SFMLView.changeStream(None, input_files[0])
                print("video chosen")

    def VisualizeNetwork(self):
        if self.eval_comboBox_choose_camera.currentText() and self.eval_pushButton_visualize.text() == "Run visualization":
            self.SFMLView.on_update_get_outputs_pause = False
            self.AllowFeatures(True)
        else:
            self.SFMLView.on_update_get_outputs_pause = not (self.SFMLView.on_update_get_outputs_pause)
            self.PauseVisualization()

    def PauseVisualization(self):
        if self.SFMLView.on_update_get_outputs_pause == False:
            self.eval_pushButton_visualize.setText("Pause visualization")

        else:
            self.eval_pushButton_visualize.setText("Run visualization")

        # input_file = self.dlg.openFileNameDialog(self.working_dir)

    def RefreshCameras(self):
        devices = {}
        self.use_camera = True
        if platform == "linux" or platform == "linux2":

            device_re = re.compile("Bus\s+(?P<bus>\d+)\s+Device\s+(?P<device>\d+).+ID\s(?P<id>\w+:\w+)\s(?P<tag>.+)$", re.I)
            df = subprocess.check_output("lsusb")
            for i in df.splitlines():
                if i:
                    usb_dev_string = i.decode("utf-8")
                    info = device_re.match(usb_dev_string)
                    # print(usb_dev_string)
                    if "cam" in usb_dev_string.lower():
                        dinfo = info.groupdict()
                        dinfo['device'] = '/dev/bus/usb/%s/%s' % (dinfo.pop('bus'), dinfo.pop('device'))
                        devices.update({dinfo["tag"]: dinfo})
            # directly open camera and create and take and image

            # self.eval_comboBox_choose_camera.clear()

            for every_device in devices.keys():
                self.eval_comboBox_choose_camera.addItem(every_device)
            current_cam = self.eval_comboBox_choose_camera.currentText()

            if current_cam:
                # camID = devices[current_cam]['device']
                cap = cv2.VideoCapture(-1)
                # print(cap)
                print(cap.isOpened())
                if cap.isOpened():
                    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)  # float
                    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float
                    ret, frame = cap.read()
                    frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
                    bytesPerLine = 3 * int(width)
                    p_video = QImage(frame.data, int(width), int(height), bytesPerLine, QImage.Format_RGB888)
                    p = QPixmap.fromImage(p_video)
                    self.RefreshVideo(p.scaled(240, 180, Qt.KeepAspectRatio))

                self.SFMLView.changeStream(0, None)


        #windows
        else:

            for i in range(10):
                cam = cv2.VideoCapture(i)
                if cam.isOpened():
                    devices.update({"camera "+str(i): i})
            # directly open camera and create and take and image


            # self.eval_comboBox_choose_camera.clear()
            for every_device in devices.keys():
                self.eval_comboBox_choose_camera.addItem(every_device)
            current_cam = self.eval_comboBox_choose_camera.currentText()

            if current_cam:

                cap = cv2.VideoCapture(devices[current_cam])

                if cap.isOpened():
                    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)  # float
                    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float
                    ret, frame = cap.read()
                    frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
                    bytesPerLine = 3 * int(width)
                    p_video = QImage(frame.data, int(width), int(height), bytesPerLine, QImage.Format_RGB888)
                    p = QPixmap.fromImage(p_video)
                    self.RefreshVideo(p.scaled(240, 180, Qt.KeepAspectRatio))

                self.SFMLView.changeStream(0, None)

        print(devices)




    def SaveFile(self):
        temp_text = QtCore.QDateTime.currentDateTime().toString() + "You just saved file"

    def UpdateCanvasTextures(self, list_of_layers):
        # pause the visualization
        self.SFMLView.on_update_get_outputs_pause = True
        # reinit the textures
        self.SFMLView.initTextures(0, list_of_layers, self.row)
        self.UpdateCanvasTextures(list_of_layers)
        geometry = self.eval_frame.geometry()
        self.SFMLView.resizeCanvas(QPoint(1, 1), QSize(geometry.width()-2, geometry.height()-2))
        self.SFMLView.on_update_get_outputs_pause = False

    def BenchMarkTestVideo(self, video_path, list_of_layers, monitor_timer):
        self.SFMLView.changeStream(None, video_path)

        self.SFMLView.on_update_get_outputs_pause = False
        self.AllowFeatures(True)
        self.eval_pushButton_visualize.setText("Pause visualization")

        self.UpdateCanvasTextures(list_of_layers)
        # Instantiate monitor with a 10-second delay between updates
        self.monitor = Monitor.Monitor(monitor_timer)
        self.SFMLView.grad_cam = True
        self.SFMLView.selected_layer_name = 'block_3_depthwise'

    def BenchMarkTestImages(self, images_path, list_of_layers, monitor_timer):
        self.test_set = self.DataSet.loadTestSetFromPath(images_path)

        self.SFMLView.testSet = self.test_set

        self.UpdateCanvasTextures(list_of_layers)
        # Instantiate monitor with a 10-second delay between updates
        self.monitor = Monitor.Monitor(monitor_timer)

    def BenchMarkTestWebCam(self, list_of_layers, monitor_timer):
        self.SFMLView.on_update_get_outputs_pause = False
        self.AllowFeatures(True)
        self.eval_pushButton_visualize.setText("Pause visualization")

        self.UpdateCanvasTextures(list_of_layers)
        # Instantiate monitor with a 10-second delay between updates
        print("Now start benchmarking")
        self.monitor = Monitor.Monitor(monitor_timer)

    def BenchmarkReport(self):
        fps_mean = np.mean(np.asarray(self.inference_time))
        print("Average fps: {}".format(fps_mean))

    def ExitApp(self):
        # Close monitor
        self.monitor.stop()
        sys.exit(app.exec_())


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.setWindowTitle("CNN Evaluating Framework")
    window.showMaximized()
    sys.exit(app.exec_())

