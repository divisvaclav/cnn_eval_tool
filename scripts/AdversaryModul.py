from __future__ import print_function

import os
from tqdm import tqdm
import numpy as np
import copy
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
from tensorflow.keras.applications import mobilenet_v2, resnet_v2, vgg16, efficientnet

import GeneralFunctions as GeneralFunctions

# TF settings for faster operation
config = tf.config.experimental
config.set_memory_growth = ('GPU', True)
keras.backend.set_learning_phase(0)

fun = GeneralFunctions.GeneralFunctions()

loss_object = tf.keras.losses.CategoricalCrossentropy()

pretrained_model_list = list()
decode_predictions_list = list()
models_name_list = list()

pretrained_model_list.append(mobilenet_v2.MobileNetV2(include_top=True,
                                                      weights='imagenet'))
decode_predictions_list.append(mobilenet_v2.decode_predictions)
models_name_list.append("MobileNetV2")

pretrained_model_list.append(vgg16.VGG16(include_top=True,
                                         weights='imagenet'))
decode_predictions_list.append(vgg16.decode_predictions)
models_name_list.append("VGG16")

pretrained_model_list.append(resnet_v2.ResNet50V2(include_top=True,
                                                  weights='imagenet'))
decode_predictions_list.append(resnet_v2.decode_predictions)
models_name_list.append("ResNet50V2")

pretrained_model_list.append(efficientnet.EfficientNetB0(input_shape=None, include_top=True, weights="imagenet",
                                                         input_tensor=None, pooling=None, classes=1000))
decode_predictions_list.append(efficientnet.decode_predictions)
models_name_list.append("EfficientNetB0")


# Helper function to preprocess the image so that it can be inputted in MobileNetV2
def preprocess(_image, _models_name):
  _image = tf.cast(_image, tf.float32)
  _image = tf.image.resize(_image, (224, 224))
  if _models_name == "VGG16":
    # images from uint8 range, centered around zero
    _image = vgg16.preprocess_input(_image)
  elif _models_name == "ResNet50V2":
    _image = resnet_v2.preprocess_input(_image)
  elif _models_name == "MobileNetV2":
    _image = mobilenet_v2.preprocess_input(_image)
  elif _models_name == "EfficientNetB0":
    # images from 0 - 255
    _image = efficientnet.preprocess_input(_image)
  else:
    print("Model not supported")
  _image = _image[None, ...]
  return _image

# Helper function to extract labels from probability vector


def get_imagenet_label(probs):
  return decode_predictions(probs, top=1)[0][0]


def clip_image_based_on_model(_model, _image):
  if _model == "VGG16":
    _image = tf.clip_by_value(_image, -255, 255)
  elif _model == "EfficientNetB0":
    _image = tf.clip_by_value(_image, 0, 255)
  else:
    # for all other models clip between -1 and 1
    _image = tf.clip_by_value(_image, -1, 1)
  return _image


def create_adversarial_pattern(input_image, input_label, epsilon, _model):
  with tf.GradientTape() as tape:
    tape.watch(input_image)
    prediction = pretrained_model(input_image)
    loss = loss_object(input_label, prediction)

  # Get the gradients of the loss w.r.t to the input image.
  gradient = tape.gradient(loss, input_image)
  # Get the sign of the gradients to create the perturbation
  signed_grad = tf.sign(gradient)

  adv_x = image + epsilon * signed_grad

  clip_image_based_on_model(_model, adv_x)

  _, label, confidence = get_imagenet_label(pretrained_model.predict(adv_x))

  return adv_x, label, confidence


def display_images(_image, description):
  _, label, confidence = get_imagenet_label(pretrained_model.predict(_image))
  plt.figure()
  plt.imshow(_image[0]*0.5+0.5)
  plt.title('{} \n {} : {:.2f}% Confidence'.format(description,
                                                   label, confidence*100))
  plt.show()


def rescale_image_based_on_model(_model, _image, _original_img):
  if _model == "VGG16":
    # images from uint8 range, centered around zero
    temp_img_np = copy.deepcopy(np.asarray(_image))
    mean = [0, 0, 0]
    std = [0, 0, 0]
    rescaled_img_np = np.ones(temp_img_np.shape)

    for i in range(3):
      mean[i] = np.mean(temp_img_np[:, :, i])
      std[i] = np.std(temp_img_np[:, :, i])

    for i in range(3):
      rescaled_img_np[:, :, 2-i] = temp_img_np[:, :, i] * std[i]
      rescaled_img_np[:, :, 2-i] = temp_img_np[:, :, i] + mean[i]

    _image_np = (rescaled_img_np / 255.0) + 0.5
    _image_np = np.clip(_image_np, 0.0, 1.0)

  elif _model == "EfficientNetB0":
    _image = tf.clip_by_value(_image, 0, 255)
    _image_np = np.asarray(_image / 255.0)

  else:
    # for all other models clip between -1 and 1
    _image = tf.clip_by_value(_image, -1, 1)
    _image_np = np.asarray(_image * 0.5 + 0.5)

  return _image_np


tested_classes = ["mountain_bike"]
third = "D:\\PycharmProjects"
path_dataset = os.path.join(third, "cnn_eval_tool", "data", "dataset", "reduced_validation_set")
path_adversary_samples = os.path.join(third, "cnn_eval_tool", "data", "dataset", "adversary")
path_classes = os.path.join(third, "cnn_eval_tool", "data", "dataset", "imagenet1000_clsidx_to_labels.txt")
list_of_classes_labels = fun.openTxtAsList(path_classes)

for index, all_models in enumerate(models_name_list):

  pretrained_model = pretrained_model_list[index]
  pretrained_model.trainable = False
  models_name = all_models
  decode_predictions = decode_predictions_list[index]
  print("Attacking model: {}".format(all_models))

  for all_classes in tested_classes:
    files_path = os.path.join(path_dataset, all_classes)
    files = [x for x in os.listdir(files_path) if not x.startswith('.')]
    files.sort()
    # files = files[:1] # one sample just for testing
    print("Attacking class: {}".format(all_classes))

    class_id = list_of_classes_labels.index(all_classes)
    if models_name == "VGG16" or models_name == "EfficientNetB0":
      epsilons = 2.0
    else:
      epsilons = 0.02

    adv_temp_path = os.path.join(path_adversary_samples, models_name, all_classes)
    if not os.path.exists(adv_temp_path):
      os.makedirs(adv_temp_path)

    for file_name in tqdm(files):
      image_raw = tf.io.read_file(os.path.join(files_path, file_name))
      original_image = tf.image.decode_image(image_raw)

      image = preprocess(original_image, models_name)
      image_probs = pretrained_model.predict(image)

      # Get the input label of the image.
      label = tf.one_hot(class_id, image_probs.shape[-1])
      label = tf.reshape(label, (1, image_probs.shape[-1]))

      # first pertubation
      adv_x, _, __ = create_adversarial_pattern(image, label, epsilons, models_name)

      new_label = all_classes
      class_confidence = 0.0
      condition = True
      iterations = 0

      while condition and iterations < 20:
        if new_label == all_classes:
          condition = True
        else:
          condition = class_confidence < 0.5
        """ stopping condition has to be different class from original """
        adv_x, new_label, class_confidence = create_adversarial_pattern(adv_x, label, epsilons, models_name)

        iterations += 1

      adversary_image_name = file_name.split(".")[:-1]
      temp_name = ""
      for chunks in adversary_image_name:
        temp_name += chunks
      adversary_image_name = "adv_" + temp_name + ".png"
      rescaled_image = rescale_image_based_on_model(models_name, adv_x[0], original_image[0])
      plt.imsave(os.path.join(adv_temp_path, adversary_image_name), rescaled_image)