# coding=utf-8
from __future__ import division
import random
import os

from PySide6.QtGui import QColor
from PySide6.QtWidgets import QTreeWidgetItem, QTableWidgetItem, QDoubleSpinBox
from PySide6 import QtWidgets, QtGui
from PySide6.QtCore import Qt


def setKey(dictionary, key, value):
    if key not in dictionary:
        dictionary[key] = [value]
    elif isinstance(dictionary[key], list):
        dictionary[key].append(value)
    else:
        dictionary[key] = [dictionary[key], value]


def proveListEmptiness(full_list, index):
    if full_list:
        if len(full_list) < index:
            return None
        else:
            return full_list[index]
    else:
        return None


def removeItem(dictionary, key):
    del dictionary[key]


def generateColor(num_of_colors):
    color = []
    listOfRGB = [[random.sample(range(256), k=3)] for i in range(num_of_colors)]
    for rgb in listOfRGB:
        color.append(QColor(rgb[0][0], rgb[0][1], rgb[0][2], 127))
    return color


def generateGreenToRedPallete():
    color = []
    for n in range(101):
        R = (255 * (100 - n)) / 100
        G = (255 * n) / 100
        B = 0
        color.append(QColor(int(R), int(G), int(B), 127))
    return color


def treeWidgetAddItemsFromList(treeWidget, lst):
    for text in lst:
        text = text.split(os.sep)[-1]
        parent = QTreeWidgetItem(treeWidget)
        parent.setText(0, "{}".format(text))
        parent.setFlags(parent.flags())


def treeWidgetAddItemsFromDict(treeWidget, dictionary):
    for keys, val in sorted(dictionary.items()):
        parent = QTreeWidgetItem(treeWidget)
        parent.setText(0, "{}".format(keys))
        parent.setFlags(parent.flags() | Qt.ItemIsUserCheckable)
        parent.setCheckState(0, Qt.Unchecked)
        for childVal in val:
            child = QTreeWidgetItem(parent)
            child.setFlags(child.flags())
            child.setText(0, "{}".format(childVal))


def createSpinBox(layout, value, pos_x, pos_y):
    spin_box = QDoubleSpinBox()
    spin_box.setDecimals(3)
    spin_box.setMaximum(10.0)
    spin_box.setMinimum(-10.0)
    spin_box.setSingleStep(0.001)
    spin_box.setValue(value)
    layout.addWidget(spin_box, pos_x, pos_y)


def createTableContent(table, rows, columns, maxi, sum_array):
    table.setRowCount(rows)
    table.setColumnCount(columns)
    for i in range(rows):
        for j in range(columns):
            item = QTableWidgetItem(str(i*columns + j))

            item.setTextAlignment(Qt.AlignCenter)
            alfa = sum_array[i*columns + j] * 255 / maxi
            item.setBackground(QtGui.QColor(0, int(alfa), 0))
            table.setItem(i, j, item)

            header = table.horizontalHeader()
            header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
