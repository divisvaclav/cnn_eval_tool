import scripts.GeneralFunctions as GeneralFunctions
from torchmetrics.classification import Accuracy
from torchvision import transforms
import torch
import os
import json
import shutil
import math
import numpy as np
from tqdm import tqdm
import torch.nn as nn
from torchvision import models

from tensorflow.keras.models import model_from_json
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class PytWrapper:
    def __init__(self):
        self.input_image = None
        self.input_image_np = None

        self.model = None
        self.custom_layers_model = None

        self.list_of_layers = list()
        self.list_of_layers_names = list()

        self.dict_of_layers = {}
        self.dict_of_weights = {}

        self.Functions = GeneralFunctions.GeneralFunctions()

        self.list_of_classes_labels = self.Functions.openTxtAsList(
            os.path.join("data", "dataset", "imagenet1000_clsidx_to_labels.txt"))

        self.num_classes = len(self.list_of_classes_labels)  # -- Number of classes

        self.list_of_diagnoses_labels = self.Functions.openTxtAsList(
            os.path.join("data", "dataset", "diagnoses_clsidx_to_labels.txt"))

        self.num_diag = len(self.list_of_diagnoses_labels)

        self.output_layer = "class_output"
        self.transform = None

    def calculate_accuracy(self, files_path, list_of_images, ground_truth_class):
        list_of_predicted_confidences = list()
        list_of_classes = [0.0] * 1000
        index = self.list_of_classes_labels.index(ground_truth_class)
        list_of_classes[index] = 1.0
        valid_accuracy = Accuracy(compute_on_step=False)

        for image_name in tqdm(list_of_images):
            self.input_image_np, _ = self.Functions.load_image(
                os.path.join(files_path, image_name), widht=224, height=224, normalize=False)

            self.input_image_np = self.transform(self.input_image_np)
            output = self.model(self.input_image_np)
            valid_accuracy(np.asarray(list_of_classes), output)
            list_of_predicted_confidences.append(output[index])

        print("Models accuracy {}".format(valid_accuracy.compute()))
        return valid_accuracy.compute()

    def open_custom_model(self, _model_path, _weight_path, _remove_diag=False):
        json_file = open(_model_path, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = model_from_json(loaded_model_json)

        # load weights from the model
        self.model.load_weights(_weight_path)
        print("Model was successfully loaded from disk {}".format(_weight_path))

        if _remove_diag:
            new_model = nn.Sequential()
            for _, module_child in self.model.named_modules():
                for _, layer in module_child.named_children():
                    if "diag" not in layer.__class__.__name__:
                        print(layer.__class__.__name__)
                        new_model.add_module(layer.__class__.__name__, layer)
                new_model.summary()

        else:
            print(self.model)
            self.dict_of_layers, self.dict_of_weights = self.get_layers_and_weights(self.model)

    def preprocess_input_for_used_model(self, image):
        self.transform = transforms.Compose([
                         transforms.Resize(256),
                         transforms.CenterCrop(224),
                         transforms.ToTensor(),
                         transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                              std=[0.229, 0.224, 0.225]
                                              )])
        image = self.transform(image)
        return image

    def get_layers_split(self, how_many_outputs=2):
        list_layers = list()
        list_weights = list()
        for _, module_child in self.model.named_modules():
            for layer_name, layer in module_child.named_children():
                if isinstance(layer, nn.Sequential):
                    if type(layer) == nn.Linear or type(layer) == nn.Conv2d:
                        for _, layer_child in layer.named_children():
                            list_layers.append(layer_child)
                            list_weights.append(layer_child.weight)

                else:
                    if type(layer) == nn.Linear or type(layer) == nn.Conv2d:
                        list_layers.append(layer)
                        list_weights.append(layer.weight)

        output_layers = [layer for layer in list_layers if
                         "conv2d" in layer.__class__.__name__.lower() and
                         layer.weight.shape[3] != 1 and layer.weight.shape[2] != 1]

        if how_many_outputs == "all":
            part = 1
        else:
            part = math.floor(len(output_layers) / how_many_outputs)

        length = len(output_layers)
        required_layers = list()
        print(length)
        print(part)
        print(output_layers)
        required_layers_temp = [output_layers[layers_index - 1] for layers_index in range(1, length, part)]
        for layer in required_layers_temp:
            required_layers.append(layer)
        last_layer = list_layers[-1]
        required_layers.append(last_layer)
        print(required_layers)
        return required_layers

    def open_resnet18(self, pretrained=False, how_many_outputs="all"):
        self.model = models.resnet18(pretrained).to(device)
        print(self.model)
        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers


    def open_alexanet(self,pretrained=False, how_many_outputs="all"):
        self.model = models.alexnet(pretrained).to(device)
        print(self.model)
        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers


    def open_vgg16(self,pretrained=False, how_many_outputs="all"):
        self.model = models.vgg16(pretrained).to(device)
        print(self.model)
        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers


    def open_squeezenet1_0(self,pretrained=False, how_many_outputs="all"):
        self.model = models.squeezenet1_0(pretrained).to(device)
        print(self.model)
        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers


    def open_densenet161(self,pretrained=False, how_many_outputs="all"):
        self.model = models.densenet161(pretrained).to(device)
        print(self.model)
        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers


    def open_inception_v3(self, pretrained=False, how_many_outputs="all"):
        self.model = models.inception_v3(pretrained).to(device)
        print(self.model)
        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_googlenet(self, pretrained=False, how_many_outputs="all"):
        self.model = models.googlenet(pretrained).to(device)
        print(self.model)
        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_mobilenet_v2(self, pretrained=False, how_many_outputs="all"):
        self.model = models.mobilenet_v2(pretrained).to(device)

        print(self.model)
        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_resnext50_32x4d(self, pretrained=False, how_many_outputs="all"):
        self.model = models.resnext50_32x4d(pretrained).to(device)
        print(self.model)
        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_imageNet_validationSet_labels(self, labels_path):
        f = open(labels_path)
        real_labels = json.load(f)
        return real_labels

    def split_imageNet_validationSet_overClass(self, labels_path, validation_path, split_path):
        imagenet_classes = self.list_of_classes_labels
        f = open(labels_path)
        real_labels = json.load(f)

        # create a list of all ImageNet validation images
        files = [x for x in os.listdir(validation_path) if not x.startswith('.')]
        files.sort()

        # create dir for all classes
        for imageNet_class in imagenet_classes:
            class_path = os.path.join(split_path, imageNet_class)
            if not os.path.exists(class_path):
                os.mkdir(class_path)

        # iterate through all images and copy image to related class directory
        for label_index, image in enumerate(files):
            # list can be empty - exclude it from splitting
            if real_labels[label_index]:
                real_label = real_labels[label_index][0]
                if isinstance(real_label, str):
                    dst_class = imagenet_classes[real_label.split(",")[-1]]
                else:
                    dst_class = imagenet_classes[real_label]
                dst = os.path.join(split_path, dst_class)
                shutil.copy2(os.path.join(validation_path, image), dst)
                print("Copying {} to {}".format(image, dst))

    def get_weights_and_layers(self):
        dict_of_layers = {}
        dict_of_weights = {}
        # append all the conv layers and their respective wights to the list
        for _, module_child in self.model.named_modules():
            for _, layer in module_child.named_children():
                print(layer)
                if isinstance(layer, nn.Sequential):
                    for _, layer_child in layer.named_children():
                        if isinstance(layer_child, nn.Linear) or isinstance(layer_child, nn.Conv2d):
                            layer_name = layer_child.__class__.__name__
                            weights = layer_child.weight
                            if str(layer_child.__class__.__name__) == "BatchNormalization":
                                print("Visualization of BatchNorm layer is not supported.")
                            else:
                                dict_of_layers.update({layer_name: {"Name": str(layer_child.__class__.__name__),
                                                                    "Weights shape": str(weights[0].shape)}
                                                       })

                                dict_of_weights.update({str(layer_name): weights})
                else:
                    if isinstance(layer, nn.Linear) or isinstance(layer, nn.Conv2d):
                        layer_name = layer.__class__.__name__
                        weights = layer.weight
                        if str(layer.__class__.__name__) == "BatchNormalization":
                            print("Visualization of BatchNorm layer is not supported.")
                        else:
                            dict_of_layers.update({layer_name: {"Name": str(layer.__class__.__name__),
                                                                "Weights shape": str(weights[0].shape)}
                                                   })

                            dict_of_weights.update({str(layer_name): weights})
        return dict_of_layers, dict_of_weights

    def getConv2D_weights_and_layers(self):
        # we will save the conv layer weights in this list
        model_weights = []
        # we will save the 49 conv layers in this list
        conv_layers = []
        # get all the model children as list
        model_children = list(self.model.children())
        # counter to keep count of the conv layers
        counter = 0
        for _, module_child in self.model.named_modules():
            if isinstance(module_child, nn.Conv2d):
                counter += 1
                model_weights.append(module_child.weight)
                conv_layers.append(module_child)
            elif isinstance(module_child, nn.Sequential):
                for _, layer in module_child.named_children():
                    for child in layer.children():
                        if type(child) == nn.Conv2d:
                            counter += 1
                            model_weights.append(child.weight)
                            conv_layers.append(child)
        print(f"Total convolution layers: {counter}")
        print("conv_layers")
        return conv_layers, model_weights

    def get_outputs_from_layers(self, _input_image, _feature_map_dict, _layers):
        outputs = []
        names = []
        for layer in _layers[0:]:
            image = layer(image)
            outputs.append(image)
            names.append(str(layer))
        print(len(outputs))

        # print feature_maps
        for feature_map in outputs:
            print(feature_map.shape)


        return None

    def get_weights_from_model(self, list_of_layers, list_of_weights):
        _dict_of_weights = {}
        for index, layer_name in enumerate(list_of_layers):
            weights = list_of_weights[layer_name]
            _dict_of_weights.update({str(layer_name): weights[0]})

    # ToDo
    def get_model_with_costum_layer(self, _list_of_layers, _custom_model=True):
        self.list_of_layers = list()
        self.list_of_layers_names = list()
        self.dict_of_layers = dict()
        self.dict_of_weights = dict()
        self.custom_layers_model = None

        list_layers = list()
        list_weights = list()
        for _, module_child in self.model.named_modules():
            for layer_name, layer in module_child.named_children():
                if isinstance(layer, nn.Sequential):
                    if type(layer) == nn.Linear or type(layer) == nn.Conv2d:
                        for _, layer_child in layer.named_children():
                            list_layers.append(layer_child)
                            list_weights.append(layer_child.weight)

                else:
                    if type(layer) == nn.Linear or type(layer) == nn.Conv2d:
                        list_layers.append(layer)
                        list_weights.append(layer.weight)

        for layer in list_layers:
            layer_name = layer.__class__.__name__
            if layer_name in _list_of_layers:
                self.list_of_layers.append(layer)
                self.list_of_layers_names.append(layer_name)
                if isinstance(layer, nn.Conv2d) or isinstance(layer, nn.Linear):
                    self.dict_of_layers.update({str(layer_name): layer})
                    weights = layer.weight
                    self.dict_of_weights.update({str(layer_name): weights})

        if _custom_model:
            # create a new instance of model, with only desired layers
            self.custom_layers_model = models.Model(inputs=self.model.inputs, outputs=self.list_of_layers)
        else:
            self.custom_layers_model = models.Model(inputs=self.model.inputs, outputs=self.list_of_layers[-1])

        return None


