import GPUtil
from threading import Thread
import time
import psutil
import collections
import numpy as np


class Monitor(Thread):

    def __init__(self, delay):
        super(Monitor, self).__init__()
        self.stopped = False
        self.delay = delay
        self.attrDict = collections.defaultdict(list)
        self.attrList = list()
        self.cpu_per = list()
        self.mem_list = list()
        self.counter = 0
        self.start()

    def run(self):
        while not self.stopped and self.counter < 12:
            GPUtil.showUtilization(all=True, attrList=self.attrList, useOldCode=True, attrDict=self.attrDict)
            # gives a single float value
            self.cpu_per.append(psutil.cpu_percent())
            # you can convert that object to a dictionary
            mem = psutil.virtual_memory()
            self.mem_list.append(mem.used)
            self.counter = self.counter + 1
            print("Saving benchmark data {} [s]".format(self.counter))
            time.sleep(self.delay)

        print("End of benchmark")
        cpu_mean = np.mean(np.asarray(self.cpu_per))
        print("Average CPU load: {}".format(cpu_mean))

        gpu0_mean = np.mean(np.asarray(self.attrDict[0][0]))
        gpu1_mean = np.mean(np.asarray(self.attrDict[1][0]))
        print("Average GPU1 load: {}".format(gpu0_mean))
        print("Average GPU2 load: {}".format(gpu1_mean))
        gpu0_mem_mean = np.mean(np.asarray(self.attrDict[0][1]))
        gpu1_mem_mean = np.mean(np.asarray(self.attrDict[1][1]))
        print("Average GPU1 memory load: {}".format(gpu0_mem_mean))
        print("Average GPU2 memory load: {}".format(gpu1_mem_mean))

        ram_mem_mean = np.mean(np.asarray(self.mem_list))
        print("Average RAm memory load: {}".format(ram_mem_mean))

    def stop(self):
        self.stopped = True
        self.attrDict = collections.defaultdict(list)
        self.attrList = list()
        self.cpu_per = list()
        self.mem_list = list()
