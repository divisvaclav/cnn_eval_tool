from numba import jit
import numpy as np
import os
from sfml import sf
from keras import backend as K
import math
import copy
import time
import cv2

import matplotlib.pyplot as plt

plt.rcParams.update({'figure.max_open_warning': 0})

from bokeh.plotting import figure
from bokeh.driving import linear
from PySide6.QtGui import QImage, QPixmap
from PySide6.QtCore import Signal, Qt
import scripts.DataSets as DataSets
import scripts.GeneralFunctions as GeneralFunctions
from scripts.QsfmlCanvas import QSFMLCanvas


class CNNCanvas(QSFMLCanvas):

    signal_video_changed = Signal(QPixmap)

    def __init__(self, parent, position, size, loaded_model, additional_info, stream_source):
        super().__init__(parent, position, size, additional_info, stream_source)
        self.Safety = None
        self.loaded_model = loaded_model

    def setModel(self, model):
        self.loaded_model = model

    def onInit(self):
        self.sfml_window_width = 1920/2
        self.sfml_window_heigth = 1080/2
        self.sfml_window_margin = 40
        self.sfml_font = sf.Font.from_file("data/fonts/sansation.ttf")
        self.sfml_font_size = 10
        self.bytesPerLine = 3 * 640
        self.y_classes = [1.0]
        self.x_time = [0]
        self.additional_info = False
        self.num_of_max_activations = 5
        self.alfa = 16
        self.fading_height_pro_layer = 80
        self.max_height_pro_layer = 400
        self.max_height_pro_layer_diag = 360
        self.conv_scale_x = 16
        self.conv_scale_y = 8
        self.neurons_radius = 8
        self.neurons_margin_x = 6
        self.neurons_margin_y = 12
        self.num_of_neurons_column = 5
        self.sf_input_img_sprite = None
        self.sf_input_img_texture = None
        self.dict_of_denses = {}
        self.dict_of_layer_labels = {}
        self.dict_of_class_labels = {}
        self.dict_of_confidences = {}
        self.dict_of_convs_texture = {}
        self.dict_of_convs_sprite = {}
        self.list_of_confidences = []
        self.list_of_layer_labels = []
        self.dict_of_denses_diag = {}
        self.dict_of_convs_texture_diag = {}
        self.dict_of_convs_sprite_diag = {}
        self.list_of_confidences_diag = []
        self.list_of_graphs_texture = []
        self.list_of_graphs_sprite = []

        self.max_responses_dict = {}
        self.max_responses_dict_diag = {}

        self.layers = list()
        self.layers_names = list()
        self.dict_of_layers = {}
        self.dict_of_weights = {}
        self.diag_layers = {}

        self.init_pos_x = 32
        self.init_pos_y = 32
        self.x_off_conv = 24
        self.x_off_dense = 24
        self.x_off_output = 32
        self.x_off_output_text = 42
        self.layers_y_margin = 64
        self.visualize_everything = False

        self.list_of_visualized_layers = list()
        self.list_of_visualized_layers_diag = list()

        self.masked_layer = list()
        self.list_of_masked_filters = list()

        self.list_of_visualized_layers_diag = ['diag_conv', 'diag_depth_conv', 'diag_dense', 'diag_output']
        self.list_of_conv_layers = ["conv", "relu", "block", "branch"]
        self.list_of_dense_layers = ["dense"]
        self.list_of_softmax_layers = ["output", "logits"]

        self.DataSet = DataSets.DataSet()
        self.GenFun = GeneralFunctions.GeneralFunctions()

        self.list_of_classes_labels = self.GenFun.openTxtAsList(
            os.path.join("data", "dataset", "pedestrian_clsidx_to_labels.txt"))

        self.num_classes = len(self.list_of_classes_labels)  # -- Number of classes
        self.list_of_diagnoses_labels = self.GenFun.openTxtAsList(
            os.path.join("data", "dataset", "diagnoses_clsidx_to_labels.txt"))
        self.num_diag = len(self.list_of_diagnoses_labels)
        self.output_layer = "class_output"
        self.selected_layer_name = "None"
        self.grad_cam = False
        self.noise_mean = 0.0
        self.noise_std = 0.0
        self.normal_noise = np.random.normal(loc=0.0, scale=0.0, size=(224, 224, 3))

        # Set up plot
        self.plot = figure(plot_height=400, plot_width=800, title="confidence",
                           tools="crosshair,pan,reset,save,wheel_zoom",
                           x_range=[0, 200], y_range=[-2.5, 2.5])

        self.line = self.plot.line('x', 'y', line_width=2, line_alpha=0.6)
        self.data_source = self.line.data_source
        self.viridis_color_map = [
            [68, 1, 84, 1], [71, 39, 117], [62, 72, 135],
            [49, 102, 141], [38, 130, 141], [36, 157, 136],
            [55, 181, 120], [109, 204, 88], [176, 221, 49],
            [253, 231, 37]]

        self.cap = None
        self.cap = cv2.VideoCapture(0)

        width = 224
        height = 224
        channels = 3

        self.dim_w_channels = (width, height, channels)
        self.dim_wo_channels = (width, height)
        self.input_image = np.ndarray(shape=(1, width, height, channels), dtype=float)
        self.new_model = None

        self.inference_time = list()

        input_files = os.path.join("data", "dataset", "custom")
        self.testSet, list_of_files = self.DataSet.loadTestSetFromPath(input_files)

        if self.additional_info:
            or_img_np, or_img = self.GenFun.load_image(
                os.path.join("data", "dataset", "INRIA_Person_Dataset_256", "pedestrian", "2020-05-22-154634.jpg"),
                (224, 224))

            self.X_bars = self.DataSet.loadTestSetFromPath(
                os.path.join("data", "dataset", "ADVERSARY_Dataset_224", "pedestrian",
                             "2020-05-22-154634_target_traffic_sign", "histograms_confidence"),
                _resize=True, _width=384, _height=288, _channels=3)

            self.X_noise = self.DataSet.loadTestSetFromPath(
                os.path.join("data", "dataset", "ADVERSARY_Dataset_224", "pedestrian",
                             "2020-05-22-154634_target_traffic_sign", "weights"))

            input_img_pos_y = self.init_pos_y
            self.addImgTextureToList(or_img_np, 224, 224, self.init_pos_x, input_img_pos_y, self.list_of_graphs_texture,
                                     self.list_of_graphs_sprite)
            self.addImgTextureToList(self.X_bars[0], 384, 288, self.init_pos_x + 640,
                                     self.init_pos_y + self.max_height_pro_layer + self.layers_y_margin / 2,
                                     self.list_of_graphs_texture, self.list_of_graphs_sprite)
            self.addImgTextureToList(self.X_noise[0], 224, 224, self.init_pos_x, input_img_pos_y + 1 * 260,
                                     self.list_of_graphs_texture, self.list_of_graphs_sprite)

            self.initTextures(0, self.list_of_visualized_layers)




    def set_Safety(self, safety_constructor):
        self.Safety = safety_constructor

    def clearTextures(self):
        self.clear(sf.Color.WHITE)
        self.init_pos_x = 32
        self.init_pos_y = 32

        self.layers = list()
        self.layers_names = list()
        self.dict_of_layers = dict()
        self.dict_of_weights = dict()

        self.dict_of_convs_texture = dict()
        self.dict_of_convs_sprite = dict()
        self.dict_of_denses = dict()
        self.list_of_confidences = list()

        self.dict_of_convs_texture_diag = dict()
        self.dict_of_convs_sprite_diag = dict()
        self.dict_of_denses_diag = dict()
        self.list_of_confidences_diag = list()
        self.list_of_layer_labels = list()

    def changeStream(self, stream, path):
        if path is not None:
            self.cap = cv2.VideoCapture(path)
        else:
            self.cap = cv2.VideoCapture(stream)
            assert (self.cap.isOpened() == False, "ERR - No camera available")

        self.stream_source = True

    def initTextures(self, camera, checked_layers, row=None):

        # assert(self.cap.isOpened() == False, "ERR - No camera available")
        frame = self.testSet[0, :, :, :]
        if self.testSet is not None:
            frame = self.testSet[row, :, :, :]
        input_shape = self.loaded_model.layers[0].input_shape
        self.dim_w_channels = (input_shape[0][1], input_shape[0][2], input_shape[0][3])
        self.dim_wo_channels = (input_shape[0][1], input_shape[0][2])

        resized = cv2.resize(frame, self.dim_wo_channels)
        self.input_image = np.ndarray(shape=(1, resized.shape[0], resized.shape[1], 3), dtype=float)
        self.input_image[0, :, :, :] = resized[:, :, :3] / 255.0
        self.clearTextures()
        self.list_of_visualized_layers = checked_layers

        feature_map_dict = {}
        feature_map_diag_dict = {}
        max_responses_dict = {}
        max_responses_dict_diag = {}

        # save original weights
        self.Safety.get_outputs_from_layers(self.Safety.custom_layers_model,
                                            self.input_image, feature_map_dict,
                                            self.Safety.list_of_layers_names)

        self.Safety.calculate_max_responses(feature_map_dict, False)
        if feature_map_diag_dict.keys():
            self.Safety.calculate_max_responses(feature_map_diag_dict, max_responses_dict,  False)

        # Preparing input image texture
        if self.grad_cam:
            normalized_img = (self.input_image[0, :, :, :] - np.min(self.input_image[0, :, :, :])) / np.ptp(
                self.input_image[0, :, :, :])
            input_image_char = np.zeros((224, 224, 4))
            input_image_char[:, :, :3] = normalized_img * 255  #
            input_image_char[:, :, 3] = 255
            sf_image = sf.Image.from_pixels(224, 224,
                                            input_image_char)
            self.sf_input_img_texture = sf.Texture.from_image(sf_image)
            self.sf_input_img_sprite = sf.Sprite(self.sf_input_img_texture)
            input_img_pos_x = self.init_pos_x
            if self.additional_info:
                input_img_pos_y = self.init_pos_y + 2 * 260
            else:
                input_img_pos_y = self.init_pos_y + int(self.max_height_pro_layer / 2 - self.input_image.shape[2] / 2)

            self.sf_input_img_sprite.position = (input_img_pos_x, input_img_pos_y)
            self.init_pos_x = self.init_pos_x + self.input_image.shape[1] + self.x_off_conv

        self.prepareLayers(feature_map_dict, self.init_pos_x, self.init_pos_y,
                           self.dict_of_convs_texture,
                           self.dict_of_convs_sprite,
                           self.dict_of_denses,
                           self.list_of_classes_labels,
                           self.list_of_confidences,
                           max_responses_dict,
                           self.max_height_pro_layer)

        # Now to find the right position of diagnostic layer and align it with its CONV layer
        self.init_pos_y = self.init_pos_y + self.max_height_pro_layer + self.layers_y_margin
        self.diag_layers = [layer_names for layer_names in self.layers_names if "diag" in layer_names]

        print(self.diag_layers)
        if len(self.diag_layers) > 0:
            first_diag_layers = self.diag_layers[0]
            number_of_diag_layer = int(first_diag_layers.split("_")[2])
            print(number_of_diag_layer)

            for index_layer, layer_key in enumerate(feature_map_dict.keys()):
                if index_layer < number_of_diag_layer:
                    # print(self.init_pos_x)
                    self.init_pos_x += self.x_off_conv + feature_map_dict[layer_key].shape[0]

            self.prepareLayers(feature_map_diag_dict, self.init_pos_x, self.init_pos_y,
                               self.dict_of_convs_texture_diag,
                               self.dict_of_convs_sprite_diag,
                               self.dict_of_denses_diag,
                               self.list_of_diagnoses_labels,
                               self.list_of_confidences_diag,
                               max_responses_dict_diag,
                               self.max_height_pro_layer_diag)

    def setNoise(self, mean, std):
        self.normal_noise = np.random.normal(loc=mean, scale=std, size=(224, 224, 3))
        self.noise_mean = mean
        self.noise_std = std
        return self.normal_noise

    @linear()
    def update(self, ):
        self.data_source.trigger("data", self.data_source.data, self.data_source.data)



    @staticmethod
    def updateDense(_dense_layer, _layer_key, _dict_of_denses_texture):
        max = np.max(_dense_layer)

        for number_of_neuron in range(0, len(_dict_of_denses_texture[_layer_key])):
            alfa = _dense_layer[number_of_neuron] * 255 / max
            color = sf.Color(0, alfa, 0, 255)
            _dict_of_denses_texture[_layer_key][number_of_neuron].fill_color = color

    # def addDenseTextureToList(self, _neuron, _x, _y, _list_of_denses_texture, _neurons_radius):
    @staticmethod
    def addDenseTextureToList(_neuron, _x, _y, _list_of_denses_texture, _neurons_radius, _alfa):
        layer_radius = float(_neurons_radius - 1)
        layer_texture = sf.CircleShape()
        layer_texture.radius = layer_radius - 1
        layer_texture.outline_thickness = 1
        layer_texture.outline_color = sf.Color.BLACK
        color = sf.Color(0, 255, 0, _alfa)
        layer_texture.fill_color = color
        layer_texture.origin = (layer_radius / 2, layer_radius / 2)
        layer_texture.position = (_x, _y)
        _list_of_denses_texture.append(layer_texture)

    def prepareDense(self, _dense_layer, _layer_name, _x, _y, _dict_of_denses_texture, _max_layer_height):
        n = _dense_layer.shape[0]
        x = copy.deepcopy(_x)
        y = copy.deepcopy(_y)

        list_of_denses_texture = []
        max_num_neurons_row = int(math.floor(_max_layer_height / self.neurons_margin_y))
        max_num_neurons_row_with_fading = int(
            math.floor((_max_layer_height + 2 * self.fading_height_pro_layer) / self.neurons_margin_y))
        if max_num_neurons_row % 2 == 1:
            max_num_neurons_row = max_num_neurons_row - 1

        if n > max_num_neurons_row:
            max_num_neurons = max_num_neurons_row * self.num_of_neurons_column
            # multicolumn - less allowed colmuns than total number of neurons
            if n > max_num_neurons:
                remaining_rest = max_num_neurons_row_with_fading - max_num_neurons_row
                print(remaining_rest)
                # to prevent that there is not enough neurons to visualize
                if remaining_rest > (n - max_num_neurons):
                    remaining_rest = n - max_num_neurons

                for number_of_rows in range(0, self.num_of_neurons_column):
                    # in case
                    if number_of_rows == 0:
                        max_range = max_num_neurons_row + int(remaining_rest / 2)
                        x = _x - int(remaining_rest / 2) * self.neurons_margin_x / 2
                        y = _y - int(remaining_rest / 2) * self.neurons_margin_y / 2
                        alfa = 0
                        radius = 0
                    elif number_of_rows == (self.num_of_neurons_column - 1):
                        max_range = max_num_neurons_row + int(remaining_rest / 2)
                        alfa = 255
                        radius = self.neurons_radius
                    else:
                        max_range = max_num_neurons_row
                        alfa = 255
                        radius = self.neurons_radius

                    for number_of_neuron in range(0, max_range):
                        # progressive fading
                        if number_of_rows == 0 and number_of_neuron < int(remaining_rest / 2):
                            alfa = alfa + (255 / int(remaining_rest / 2))
                            radius = radius + (self.neurons_radius / int(remaining_rest / 2))
                            x -= self.neurons_margin_x / 2
                            y -= self.neurons_margin_y / 2
                        elif number_of_rows == (
                                self.num_of_neurons_column - 1) and number_of_neuron > max_num_neurons_row:
                            alfa = alfa - (255 / int(remaining_rest / 2))
                            radius = radius - (self.neurons_radius / int(remaining_rest / 2))
                            x -= self.neurons_margin_x / 2
                            y -= self.neurons_margin_y / 2

                        # print(number_of_neuron + number_of_rows*max_num_neurons)
                        self.addDenseTextureToList(
                            _dense_layer[number_of_neuron + number_of_rows * max_num_neurons_row],
                            x, y,
                            list_of_denses_texture,
                            radius,
                            alfa)
                        x += self.neurons_margin_x
                        y += self.neurons_margin_y

                    x = copy.deepcopy(_x)
                    x += 2 * self.neurons_margin_x * (number_of_rows + 1)
                    y = copy.deepcopy(_y)

            # multicolumn - neurons fit in predifined space
            else:
                rows = int(math.floor(n / max_num_neurons_row))
                reminder = n % max_num_neurons_row

                for number_of_rows in range(0, rows):
                    for number_of_neuron in range(0, max_num_neurons_row):
                        self.addDenseTextureToList(
                            _dense_layer[number_of_neuron + number_of_rows * max_num_neurons_row],
                            x, y,
                            list_of_denses_texture,
                            self.neurons_radius,
                            255)
                        x += self.neurons_margin_x
                        y += self.neurons_margin_y
                    x = copy.deepcopy(_x)
                    x += 2 * self.neurons_margin_x * (number_of_rows + 1)
                    y = copy.deepcopy(_y)

                for number_of_neuron in range(0, reminder):
                    self.addDenseTextureToList(_dense_layer[number_of_neuron + rows * max_num_neurons_row],
                                               x, y,
                                               list_of_denses_texture,
                                               self.neurons_radius,
                                               255)
                    x += self.neurons_margin_x
                    y += self.neurons_margin_y

        # onecolumn - neurons fit in predifined space
        else:
            for number_of_neuron in range(n):
                self.addDenseTextureToList(_dense_layer[number_of_neuron],
                                           x, y,
                                           list_of_denses_texture,
                                           self.neurons_radius,
                                           255)
                x += self.neurons_margin_x
                y += self.neurons_margin_y

        # print(list_of_convs_texture)
        _dict_of_denses_texture.update({_layer_name: list_of_denses_texture})

    def addConvTextureToList(self, _fmap, _widht, _height, _x, _y, _number_of_fmaps, _layer_name, _max_responses_dict,
                             _list_of_convs_texture, _list_of_convs_sprite, _alfa):

        #if len(_max_responses_dict.keys()) != 0:
            #if _number_of_fmaps in list(_max_responses_dict[_layer_name]):
            #    fmap_char = self.normalizeArray(_fmap, _widht, _height, 255)
            #else:
            #fmap_char = self.normalizeArray(_fmap, _widht, _height, 255)
        #else:
        fmap_char = self.normalizeArray(_fmap, _widht, _height, _alfa)

        conv_image = sf.Image.from_pixels(_widht, _height, fmap_char)
        conv_texture = sf.Texture.from_image(conv_image)
        conv_sprite = sf.Sprite(conv_texture)
        conv_sprite.position = (_x, _y)
        _list_of_convs_texture.append(conv_texture)
        _list_of_convs_sprite.append(conv_sprite)

    def prepareConv(self, _conv_layer, _layer_name, _x, _y, _dict_of_convs_texture, _dict_of_convs_sprite,
                    _max_responses_dict, _max_layer_height):
        widht = _conv_layer.shape[0]
        height = _conv_layer.shape[1]
        n = _conv_layer.shape[2]
        x = copy.deepcopy(_x)
        y = copy.deepcopy(_y)

        if widht > self.conv_scale_x:
            # mar_x = int(math.floor(widht / self.conv_scale_x))
            # mar_y = int(math.floor(height / self.conv_scale_y))
            mar_x = 5
            mar_y = 10
        else:
            mar_x = 2
            mar_y = 4

        list_of_convs_texture = []
        list_of_convs_sprite = []

        max_num_fmaps = int(math.floor(_max_layer_height / mar_y))
        max_num_fmaps_with_fading = int(math.floor((_max_layer_height + 2 * self.fading_height_pro_layer) / mar_y))
        # print("FM widht {} height {}, number {}".format(widht, height, n))
        # print("Number of feature maps {} for layer {}".format(max_num_fmaps,_layer_name))
        if max_num_fmaps_with_fading % 2 == 1:
            max_num_fmaps_with_fading = max_num_fmaps_with_fading - 1
        if max_num_fmaps % 2 == 1:
            max_num_fmaps = max_num_fmaps - 1

        if n > max_num_fmaps:

            if n < max_num_fmaps_with_fading:
                max_num_fmaps_with_fading = n

            # remaining_rest = n - max_num_fmaps_with_fading
            if self.visualize_everything == True:
                remaining_rest = n - max_num_fmaps
                if remaining_rest % 2 == 1:
                    remaining_rest = remaining_rest - 1

                # start = int(remaining_rest/2)
                start = 0
                stop = int(remaining_rest / 2 + max_num_fmaps + remaining_rest / 2)

                x = _x - (int(remaining_rest / 2) * mar_x)
                y = _y - (int(remaining_rest / 2) * mar_y)
                alfa = 0
                scaling = _conv_layer.shape[0]
            else:
                alfa = 0
                start = 0
                stop = max_num_fmaps

            for number_of_fmaps in range(start, stop):
                # if number_of_fmaps < int(remaining_rest):
                #     alfa = alfa + (128/int(remaining_rest/2))
                #     large_image = _conv_layer[:,:,number_of_fmaps]
                #     scaling = int(scaling - (widht/(remaining_rest/2)))
                #     small_image = large_image[::scaling, ::scaling]
                #     widht = small_image.shape[0]
                #     height = small_image.shape[1]
                #
                # elif number_of_fmaps > int(remaining_rest/2 + max_num_fmaps_with_fading):
                #     alfa = alfa - (128/int(remaining_rest/2))
                #     scaling = int(scaling + (widht/(remaining_rest/2)))
                #     small_image = large_image[::scaling, ::scaling]
                #     widht = small_image.shape[0]
                #     height = small_image.shape[1]
                # else:
                #     scaling = 0
                #     widht = _conv_layer.shape[0]
                #     height = _conv_layer.shape[1]
                #     small_image = _conv_layer[:,:,number_of_fmaps]
                #     alfa = 128

                self.addConvTextureToList(_conv_layer[:, :, number_of_fmaps],
                                          widht, height, x, y,
                                          number_of_fmaps, _layer_name,
                                          _max_responses_dict,
                                          list_of_convs_texture,
                                          list_of_convs_sprite,
                                          alfa)
                x += mar_x
                y += mar_y

        else:
            for number_of_fmaps in range(n):
                self.addConvTextureToList(_conv_layer[:, :, number_of_fmaps],
                                          widht, height, x, y,
                                          number_of_fmaps, _layer_name,
                                          _max_responses_dict,
                                          list_of_convs_texture,
                                          list_of_convs_sprite,
                                          128)
                x += mar_x
                y += mar_y

        # print(list_of_convs_texture)
        _dict_of_convs_texture.update({_layer_name: list_of_convs_texture})
        _dict_of_convs_sprite.update({_layer_name: list_of_convs_sprite})


    @staticmethod
    @jit(nopython=True)  # Set "nopython" mode for best performance, equivalent to @njit
    def normalizeArray(np_array, x_max, y_max, alfa):
        normalized_img = (255 * (np_array - np.min(np_array)) / (np.ptp(np_array) + 1.e-9)).astype(np.uint8)
        colored_image = np.zeros((x_max, y_max, 4), dtype=np.uint8)
        colored_image[:, :, 0] = normalized_img
        colored_image[:, :, 1] = normalized_img
        colored_image[:, :, 2] = normalized_img
        colored_image[:, :, 3] = alfa
        return colored_image

    # @staticmethod
    def updateConv(self, _fmap, _layer_key, _dict_of_convs_texture, _max_responses_dict, alfa, cmap=False):
        x_max = _fmap.shape[0]
        y_max = _fmap.shape[1]

        # using cmap according to matloplib https://matplotlib.org/examples/color/colormaps_reference.html
        if cmap == True:
            cm = plt.get_cmap('viridis')

        for number_of_fmaps in range(0, len(_dict_of_convs_texture[_layer_key])):
            if cmap == True:
                colored_image = cm(_fmap[:, :, number_of_fmaps])
            else:
                map = _fmap[:, :, number_of_fmaps]
                colored_image = self.normalizeArray(map, x_max, y_max, 255)

            if len(_max_responses_dict.keys()) != 0:
                if number_of_fmaps in list(_max_responses_dict[_layer_key]):
                    fmap_char = colored_image
                    fmap_char[:, :, 3] = 255
                else:
                    fmap_char = colored_image
                    fmap_char[:, :, 3] = alfa
            else:
                fmap_char = colored_image
                fmap_char[:, :, 3] = 255

            _dict_of_convs_texture[_layer_key][number_of_fmaps].update_from_pixels(fmap_char)

    @staticmethod
    def updateInputGraph(_input_image, _texture):
        input_image_char = np.zeros((_input_image.shape[0], _input_image.shape[1], 4), dtype=np.uint8)
        input_image_char[:, :, :3] = (_input_image[:, :, :3] * 255)
        input_image_char[:, :, 3] = 255
        _texture.update_from_pixels(input_image_char)

    @staticmethod
    def updateInputImg(_input_image, _texture):
        normalized_img = (_input_image - np.min(_input_image)) / np.ptp(_input_image)
        input_image_char = np.zeros((_input_image.shape[0], _input_image.shape[1], 4), dtype=np.uint8)
        input_image_char[:, :, :3] = normalized_img[:, :, :] * 255
        input_image_char[:, :, 3] = 255
        _texture.update_from_pixels(input_image_char)

    @staticmethod
    def updateOutputs(_list_of_confidences, _list_of_class_labels, _output_fmap):
        for label_index, label_texture in enumerate(_list_of_confidences):
            label_texture.string = str(
                _list_of_class_labels[label_index] + ": " + "%.2f" % round(_output_fmap[label_index], 2))

    @staticmethod
    def addOutputsToList(output, x, y, _class_label, _list_of_confidences, _font, _font_size):
        label_text = sf.Text()
        label_text.font = _font
        label_text.character_size = _font_size
        label_text.color = sf.Color.BLACK
        label_text.string = str(_class_label + ": " + "%.2f" % round(output, 2))
        label_text.position = (x, y)
        _list_of_confidences.append(label_text)

    @staticmethod
    def addLabelsToList(_label, _x, _y, _list_of_labels, _font, _font_size):
        label_text = sf.Text()
        label_text.font = _font
        label_text.character_size = _font_size
        label_text.color = sf.Color.BLACK
        label_text.string = str(_label)
        label_text.position = (_x, _y)
        _list_of_labels.append(label_text)

    def prepareOutputs(self, _output_fmap, _x, _y, _list_of_class_labels, _list_of_confidences, _max_layer_height):
        # x = copy.deepcopy(_x)
        # y = copy.deepcopy(_y) - int(self.sfml_font_size / 2)

        x = _x
        y = _y - int(self.sfml_font_size / 2)
        n = _output_fmap.shape[0]

        max_num_neurons = int(math.floor(_max_layer_height / self.neurons_margin_y))
        if max_num_neurons % 2 == 1:
            max_num_neurons = max_num_neurons - 1

        if n > max_num_neurons:

            for number_of_neuron in range(0, max_num_neurons):
                self.addOutputsToList(_output_fmap[number_of_neuron],
                                      x, y,
                                      _list_of_class_labels[number_of_neuron],
                                      _list_of_confidences,
                                      self.sfml_font, self.sfml_font_size)
                x += self.neurons_margin_x
                y += self.neurons_margin_y

        else:
            for number_of_neuron in range(n):
                self.addOutputsToList(_output_fmap[number_of_neuron],
                                      x, y,
                                      _list_of_class_labels[number_of_neuron],
                                      _list_of_confidences,
                                      self.sfml_font, self.sfml_font_size)
                x += self.neurons_margin_x
                y += self.neurons_margin_y
    def prepareLayers(self, _layers_dict, _x, _y, _dict_of_textures, _dict_of_sprites, _dict_of_denses,
                      _list_of_class_labels, _list_of_confidences, _max_responses_dict, _max_layer_height):
        x = _x
        y = _y

        for index_layer, layer_key in enumerate(_layers_dict.keys()):

            layer_key = layer_key
            print(layer_key)
            print(self.list_of_conv_layers)
            if any(s in layer_key.lower() for s in self.list_of_conv_layers):
                self.prepareConv(_layers_dict[layer_key],
                                 layer_key.lower(), x, y,
                                 _dict_of_textures,
                                 _dict_of_sprites,
                                 _max_responses_dict,
                                 _max_layer_height)
                # x += _layers_dict[layer_key].shape[0] * 2.5
                if index_layer % 2 == 1:
                    tmp_y = y - int(2 * self.sfml_font_size)
                else:
                    tmp_y = y - int(3 * self.sfml_font_size)
                layer_key_split = layer_key.split("_")[0:2]

                self.addLabelsToList(layer_key_split, x, tmp_y, self.list_of_layer_labels, self.sfml_font,
                                     self.sfml_font_size)

                x += int(self.x_off_conv / (self.x_off_conv / _layers_dict[layer_key].shape[0])) + \
                     _layers_dict[layer_key].shape[0]

            elif any(s in layer_key.lower() for s in self.list_of_dense_layers):
                x += self.x_off_dense  # + self.num_of_neurons_column * self.neurons_margin
                # self.neurons_margin_x = 6
                # self.neurons_margin_y = 12

                # self.neurons_margin_x = self.neurons_radius + 2
                self.prepareDense(_layers_dict[layer_key],
                                  layer_key.lower(), x, y,
                                  _dict_of_denses,
                                  _max_layer_height)
                # x += _layers_dict[layer_key].shape[0] * 2.5
                # x += self.x_off_conv + _layers_dict[layer_key].shape[0]
                if index_layer % 2 == 1:
                    tmp_y = y - int(2 * self.sfml_font_size)
                else:
                    tmp_y = y - int(3 * self.sfml_font_size)
                layer_key_split = layer_key.split("_")[0:2]
                self.addLabelsToList(layer_key_split, x, tmp_y, self.list_of_layer_labels, self.sfml_font,
                                     self.sfml_font_size)

                x += self.num_of_neurons_column * self.neurons_margin_x

            elif any(s in layer_key.lower() for s in self.list_of_softmax_layers):
                x += self.x_off_output
                # self.neurons_margin_x = 6
                # self.neurons_margin_y = 12
                self.prepareDense(_layers_dict[layer_key],
                                  layer_key.lower(), x, y,
                                  _dict_of_denses,
                                  _max_layer_height)
                x += self.x_off_output_text
                self.prepareOutputs(_layers_dict[layer_key],
                                    x, y,
                                    _list_of_class_labels,
                                    _list_of_confidences,
                                    _max_layer_height)
                if index_layer % 2 == 1:
                    tmp_y = y - int(2 * self.sfml_font_size)
                else:
                    tmp_y = y - int(3 * self.sfml_font_size)
                layer_key_split = layer_key.split("_")[0:2]
                self.addLabelsToList(layer_key_split, x, tmp_y, self.list_of_layer_labels, self.sfml_font,
                                     self.sfml_font_size)

            else:
                print("Layer {}, is not supported".format(layer_key))

    def update_data(self):
        self.source.data = dict(x=np.asarray(self.x_time), y=np.asarray(self.y_classes))

    @staticmethod
    def addImgTextureToList(self, _img, _widht, _height, _x, _y, _list_of_convs_texture, _list_of_convs_sprite):
        ## Preparing input image texture
        img_char = self.normalizeArray(_img) # issue with self
        conv_image = sf.Image.from_pixels(_widht, _height, img_char)
        conv_texture = sf.Texture.from_image(conv_image)
        conv_sprite = sf.Sprite(conv_texture)
        conv_sprite.position = (_x, _y)
        _list_of_convs_texture.append(conv_texture)
        _list_of_convs_sprite.append(conv_sprite)

    def gradCAM(self, image, _class, layer_name):
        """GradCAM method for visualizing input saliency."""
        # if layer_name in self.layers_names

        H, W = 224, 224  # Input shape, defined by the model (model.input_shape)
        print(_class)
        y_c = self.loaded_model.output[0, _class]
        conv_output = self.loaded_model.get_layer(layer_name).output
        grads = K.gradients(y_c, conv_output)[0]
        # Normalize if necessary
        # grads = normalize(grads)
        gradient_function = K.function([self.loaded_model.input], [conv_output, grads])

        output, grads_val = gradient_function([image])
        output, grads_val = output[0, :], grads_val[0, :, :, :]

        weights = np.mean(grads_val, axis=(0, 1))
        cam = np.dot(output, weights)

        # Process CAM
        cam = cv2.resize(cam, (W, H), cv2.INTER_LINEAR)
        cam = np.maximum(cam, 0)
        cam_max = cam.max()
        if cam_max != 0:
            cam = cam / cam_max

        jetcam = cv2.applyColorMap(np.uint8(255 * cam), cv2.COLORMAP_JET)
        jetcam = np.asarray(jetcam, dtype='float32') / 255.0
        return jetcam

    ''' Main Loop over every image '''

    def onUpdateGetOutputs(self):

        t = time.process_time()

        #  take as input camera stream
        if self.cap.isOpened():
            # Capture frame-by-frame
            notEnd, frame = self.cap.read()

            if not notEnd:
                # Stop visualization in case no additional frame was received
                self.on_update_get_outputs_pause = True
                return False

            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
            resized = cv2.resize(frame, self.dim_wo_channels)
            self.input_image[0, :, :, :] = resized / 255.0

        # take as input a custom image input
        else:

            if self.index_testset == self.test_set.shape[0]:
                self.index_testset = 0
                return

            self.input_image[0, :, :, :] = self.test_set[self.index_testset, :, :, :3]
            resized = cv2.resize((255.0 * self.input_image[0, :, :, :]).astype(np.uint8), self.dim_wo_channels)

            if self.additional_info:
                # print(X_bars[self.index_testset,:,:,:])
                self.updateInputGraph(self.X_bars[self.index_testset, :, :, :], self.list_of_graphs_texture[1])
                self.updateInputImg(self.X_noise[self.index_testset, :, :, :], self.list_of_graphs_texture[2])

        ''' Influencing image according to user input '''
        ### clearing R color chanel
        if self.R:
            self.input_image[0, :, :, 2] = 0.0
            resized[:, :, 0] = 0.0
        ### clearing G color chanel
        elif self.G:
            self.input_image[0, :, :, 1] = 0.0
            resized[:, :, 1] = 0.0
        ### clearing G color chanel
        elif self.B:
            self.input_image[0, :, :, 0] = 0.0
            resized[:, :, 2] = 0.0
        elif self.N:
            self.input_image[0, :, :, :] += self.normal_noise
            normal_noise = np.random.normal(loc=self.noise_mean, scale=self.noise_std, size=(self.dim_w_channels))
            resized += (255 * normal_noise).astype(np.uint8)
            resized = np.clip(resized, 0, 255)
        elif self.A:
            self.input_image[0, :, :, :] = self.input_image[0, :, :, :]
            # hi
        if self.D is not self.prev_D:
            if self.D == True:
                self.maskFilterInLayer(self.masked_layer, self.list_of_masked_filters, self.dict_of_layers,
                                       self.dict_of_weights)
            else:
                self.maskFilterInLayer(self.masked_layer, [0], self.dict_of_layers, self.dict_of_weights)
            self.prev_D = self.D

        ''' Emit signal to the GUI '''
        self.bytesPerLine = 3 * resized.shape[1]
        p_video = QImage(resized.data, resized.shape[0], resized.shape[1], self.bytesPerLine, QImage.Format_RGB888)
        p = QPixmap.fromImage(p_video)
        ratio = resized.shape[1] / 240.0
        self.signal_video_changed.emit(p.scaled(240, int(resized.shape[0] / ratio), Qt.KeepAspectRatio))

        ###
        feature_map_dict = {}
        feature_map_diag_dict = {}

        self.Safety.get_outputs_from_layers(self.Safety.model, self.input_image, feature_map_dict,
                                  self.Safety.list_of_layers_names)
        # print ("get Outputs From Layers : {}".format(time.process_time() - t), end="")

        max_responses_dict = {}
        #max_responses_dict_diag = {}
        self.calculateMaxResponses(feature_map_dict, max_responses_dict, False)

        # in case the grad cam functionality is turned on
        if self.grad_cam and self.selected_layer_name is not "None":
            predicted_class = np.argmax(feature_map_dict[self.output_layer])
            jetcam = self.gradCAM(self.input_image, predicted_class, self.selected_layer_name)
            self.updateInputImg(jetcam, self.sf_input_img_texture)

        # TBD
        for index_layer, layer_key in enumerate(feature_map_dict.keys()):

            fmap = feature_map_dict[layer_key]

            if any(s in layer_key for s in self.list_of_conv_layers):
                self.updateConv(fmap,
                                layer_key,
                                self.dict_of_convs_texture,
                                max_responses_dict,
                                self.alfa)

            elif any(s in layer_key for s in self.list_of_dense_layers):
                # print(layer_key.lower())
                self.updateDense(fmap,
                                 layer_key,
                                 self.dict_of_denses)

            elif any(s in layer_key for s in self.list_of_softmax_layers):
                self.updateDense(fmap,
                                 layer_key,
                                 self.dict_of_denses)
                self.updateOutputs(self.list_of_confidences,
                                   self.list_of_classes_labels,
                                   fmap)

                # self.data_source.data['x'].append(_index)
                # self.data_source.data['y'].append(fmap[0])

                # self.y_classes.append(fmap[0])
                # self.x_time.append(_index)

            else:
                print("Unknown layer")

        # TBD
        if self.diag_layers is not None:
            for index_layer, layer_key in enumerate(feature_map_diag_dict.keys()):
                fmap = feature_map_diag_dict[layer_key]
                self.list_of_visualized_layers = ["relu", "dense", "output", "logits"]
                if layer_key in self.list_of_conv_layers:
                    self.updateConv(fmap, layer_key,
                                    self.dict_of_convs_texture_diag,
                                    self.max_responses_dict_diag,
                                    self.alfa)

                elif layer_key in self.list_of_dense_layers:
                    self.updateDense(fmap, layer_key,
                                     self.dict_of_denses_diag)

                elif layer_key in self.list_of_softmax_layers:
                    self.updateDense(fmap, layer_key,
                                     self.dict_of_denses_diag)
                    self.updateOutputs(self.list_of_confidences_diag, self.list_of_diagnoses_labels, fmap)

                else:
                    print("Unknown layer")

        self.inference_time.append(1.0 / (time.process_time() - t))
        # inference_time = (1.0/(time.process_time() - t))
        # print('FPS: %f\r'%inference_time, end="")

        return True

    def onUpdatePaint(self):
        self.clear(sf.Color.WHITE)
        # Visualizing normal convolutional layer
        for key in self.dict_of_convs_sprite.keys():
            for each_fmap in self.dict_of_convs_sprite[key]:
                self.draw(each_fmap)

        # Visualizing dense layer
        for key in self.dict_of_denses.keys():
            for each_neuron in self.dict_of_denses[key]:
                self.draw(each_neuron)

        for each_layer_label in self.list_of_layer_labels:
            self.draw(each_layer_label)

        for each_confidence_label in self.list_of_confidences:
            self.draw(each_confidence_label)

        if self.diag_layers is not None:
            # Visualizing diag convolutional layer
            for key in self.dict_of_convs_sprite_diag.keys():
                for each_fmap_diag in self.dict_of_convs_sprite_diag[key]:
                    self.draw(each_fmap_diag)

            for key in self.dict_of_denses_diag.keys():
                for each_neuron_diag in self.dict_of_denses_diag[key]:
                    self.draw(each_neuron_diag)

            for each_confidence_label_diag in self.list_of_confidences_diag:
                self.draw(each_confidence_label_diag)

        if self.additional_info:
            for each_graph_sprite in self.list_of_graphs_sprite:
                self.draw(each_graph_sprite)

        if self.grad_cam:
            self.draw(self.sf_input_img_sprite)

    def paintEngine(self):
        # let the derived class do its specific stuff
        if not(self.on_update_get_outputs_pause):
            running = self.onUpdateGetOutputs
            if running == False:
                self.on_update_get_outputs_pause = True
                self.visualization_running_signal.emit(False)

        self.onUpdatePaint()

        # display on screen
        self.display()
