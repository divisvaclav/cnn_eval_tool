"""
Module for opening different file types
"""

from PySide6.QtWidgets import QWidget, QFileDialog


class OpenFileDialog(QWidget):
    """
    Class for opening different file types
    """
    def __init__(self):
        """Init"""
        super().__init__()
        self.title = 'Open file'
        self.left = 10
        self.top = 10
        self.width = 1024
        self.height = 768

    def openDirectoryDialog(self):
        directory = QFileDialog.getExistingDirectory(self, "Select Directory")
        return directory

    def openFileNameDialog(self, directory):
        """
        Opening text files

        param dir: Directory for selecting a file
        """
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", directory, "",
                                                  "All Files (*)", options=options)
        return fileName

    def openNetworkDialog(self):
        """Opening text files and xml files"""
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self, "QFileDialog.getOpenFileNames()", "",
                                                "Weights Files (*.h5);;Model Files (*.json);;All Files (*)", options=options)
        return files

    def openCustomMaskDialog(self):
        """Opening text files and xml files"""
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self, "QFileDialog.getOpenFileNames()", "",
                                                "Numpy Files (*.npy);;Numpy Files (*.npz);;All Files (*)", options=options)
        return files


    def openTestSetDialog(self):
        """Opening pngs and jpgs files"""
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self, "QFileDialog.getOpenFileNames()", "",
                                                "Image files (*.jpg, *.png);;Video file (*.avi, *.mp4);;All Files (*)", options=options)
        return files

    def saveFileDialog(self):
        """Save text files"""
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self, "QFileDialog.getSaveFileName()", "",
                                                  "All Files (*)", options=options)
        return fileName
