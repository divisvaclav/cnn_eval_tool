from __future__ import print_function, division

from matplotlib.pyplot import imread
from PIL import Image

import os
import numpy as np
import scipy.misc as misc
import copy
from tqdm import tqdm

from tensorflow.keras.datasets import cifar10  # added
from keras.utils import np_utils


def setKey(dictionary, key, value):
    if key not in dictionary:
        dictionary[key] = [value]
    elif type(dictionary[key]) == list:
        dictionary[key].append(value)
    else:
        dictionary[key] = [dictionary[key], value]


class DataSet:
    def __init__(self, grayscale=True, labels=True, img_rows=64, img_cols=64):

        self.img_rows = img_rows
        self.img_cols = img_cols

        self.labels = labels
        if grayscale is True:
            self.channels = 1  # 3 channels RGB
        else:
            self.channels = 3  # 3 channels RGB

        self.num_classes = 0

        self.pathes = {}
        self.signs = []
        self.dataSetLen = 0
        self.minNumberOfImages = 100

    @staticmethod
    def loadTestSetFromPath(_path_dataset, _resize=True, _width=224, _height=224, _channels=3):
        x = []
        valid_images = [".jpg", ".png"]
        valid_numpy = [".npy", ".npz"]

        files = [x for x in os.listdir(_path_dataset) if not x.startswith('.')]
        files.sort()
        # print(files)

        for file_name in files:

            # import raw images and resize them
            ext = os.path.splitext(file_name)[-1]
            if ext.lower() in valid_images:
                image = Image.open(os.path.join(_path_dataset, file_name))
                if _resize:
                    rsize = image.resize(size=(_height, _width), resample=Image.BILINEAR)
                    image_array = np.asarray(rsize, dtype=np.float32)
                    image_array /= 255.
                    x.append(image_array)
            if ext.lower() in valid_numpy:
                image = np.load(os.path.join(_path_dataset, file_name))
                x.append(image)

        print("Total number of loaded images is {}".format(len(x)))

        return np.asarray(x, dtype=np.float32), files

    @staticmethod
    def loadTestSet(_list_of_images, _resize=True, _width=224, _height=224, _channels=3):
        x = []
        valid_images = [".jpg", ".png"]
        valid_numpy = [".npy", ".npz"]

        _list_of_images.sort()

        for file_name in _list_of_images:
            # import raw images and resize them
            ext = os.path.splitext(file_name)[-1]
            if ext.lower() in valid_images:
                image = Image.open(file_name)
                if _resize:
                    rsize = image.resize(size=(_height, _width), resample=Image.BILINEAR)
                    image_array = np.asarray(rsize, dtype=np.float32)
                    image_array /= 255.
                    x.append(image_array)
            if ext.lower() in valid_numpy:
                image = np.load(file_name)
                x.append(image)

        return np.asarray(x, dtype=np.float32)

    @staticmethod
    def loadImagesFromPath(_path_dataset, _train_percentage, _list_of_classes, _number_of_classes, _resize=True,
                           _width=224, _height=224, _channels=3):
        x = []
        y = []
        valid_images = [".jpg", ".png"]
        valid_numpy = [".npy", ".npz"]

        for root, dirs, _ in os.walk(_path_dataset, topdown=False):

            for dir in dirs:
                sub_dirs = os.path.join(root, dir)
                print("Loading images from {}".format(sub_dirs))
                lst = os.listdir(sub_dirs)
                lst.sort()

                for all_images in tqdm(lst):

                    file_name = os.path.join(root, dir, all_images)
                    # print(os.path.join(root, dir, all_images))
                    # print(os.path.join(root, file_name))

                    ext = os.path.splitext(file_name)[1]
                    if ext.lower() in valid_images:
                        image = Image.open(file_name)
                        # image = imread(os.path.join(file, file_name))
                        if _resize:
                            rsize = image.resize(size=(_width, _height))
                            image_array = np.asarray(rsize, dtype=np.float32)
                            image_array /= 255.
                            x.append(image_array)
                        class_array = np.zeros(_number_of_classes)
                        class_name = file_name.split(os.sep)[3]
                        class_array[_list_of_classes.index(class_name)] = 1.0
                        y.append(class_array)

                    # import numpy arrays - adversary attacks
                    if ext.lower() in valid_numpy:
                        image = np.load(file_name)
                        x.append(np.asarray(image, dtype=np.float32))

                        # print(file_name)
                        class_array = np.zeros(_number_of_classes)
                        class_name = file_name.split(os.sep)[3]
                        # print(_list_of_classes.index(class_name))
                        class_array[_list_of_classes.index(class_name)] = 1.0
                        y.append(class_array)

        print("Length of training set X {}".format(len(x)))
        print("Length of training set Y {}".format(len(y)))
        # Select a random samples and divide dataset to train and test sets
        idx_train = np.random.randint(0, len(x), int(_train_percentage / 100 * len(x)))
        # print(idx_train)
        idx_test = np.array([rest for rest in range(len(x)) if rest not in idx_train])
        # print(idx_test)
        x_train, y_train = np.array(x)[idx_train.astype(int)], np.array(y)[idx_train.astype(int)]
        x_test, y_test = np.array(x)[idx_test.astype(int)], np.array(y)[idx_test.astype(int)]

        return x_train, y_train, x_test, y_test

    def loadCIFAR10DataSet(self):
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()

        mean = [0, 0, 0]
        std = [0, 0, 0]
        num_classes = 10

        self.normalizeDataset(x_train, y_test)

        y_train = np_utils.to_categorical(y_train, num_classes)
        y_test = np_utils.to_categorical(y_test, num_classes)

        return x_train, y_train, x_test, y_test

    def loadCIFAR10TestSet(self):
        (X_train, Y_train), (X_test, Y_test) = cifar10.load_data()

        mean = [0, 0, 0]
        std = [0, 0, 0]
        num_classes = 10
        newX_test = np.ones(X_test.shape)
        for i in range(3):
            mean[i] = np.mean(X_test[:, :, :, i])
            std[i] = np.std(X_test[:, :, :, i])

        for i in range(3):
            newX_test[:, :, :, i] = X_test[:, :, :, i] - mean[i]
            newX_test[:, :, :, i] = newX_test[:, :, :, i] / std[i]

        X_test = newX_test
        Y_test = np_utils.to_categorical(Y_test, num_classes)

        return X_test, Y_test

    @staticmethod
    def normalizeTestSet(_X_test):

        mean = [0, 0, 0]
        std = [0, 0, 0]
        num_classes = 10
        newX_test = np.zeros(shape=(_X_test.shape[0], _X_test.shape[1], _X_test.shape[2], _X_test.shape[3]))
        for i in range(3):
            mean[i] = np.mean(_X_test[:,:,:,i])
            std[i] = np.std(_X_test[:,:,:,i])

        for i in range(3):
            newX_test[:,:,:,i] = _X_test[:,:,:,i] - mean[i]
            newX_test[:,:,:,i] = newX_test[:,:,:,i] / std[i]

        X_test = copy.deepcopy(_X_test)
        return X_test / 255.

    def normalizeDataset(self, _X_train, _X_test, _X_val=None, ):
        mean = [0, 0, 0]
        std = [0, 0, 0]

        print("mean before normalization: {}".format(np.mean(_X_train)))
        print("std before normalization: {}".format(np.std(_X_train)))

        newX_train = np.ones(_X_train.shape)
        newX_test = np.ones(_X_test.shape)
        newX_val = None

        for i in range(3):
            mean[i] = np.mean(_X_train[:, :, :, i])
            std[i] = np.std(_X_train[:, :, :, i])

        for i in range(3):
            newX_train[:, :, :, i] = _X_train[:, :, :, i] - mean[i]
            newX_train[:, :, :, i] = newX_train[:, :, :, i] / std[i]

        for i in range(3):
            mean[i] = np.mean(_X_test[:, :, :, i])
            std[i] = np.std(_X_test[:, :, :, i])

        for i in range(3):
            newX_test[:, :, :, i] = _X_test[:, :, :, i] - mean[i]
            newX_test[:, :, :, i] = newX_test[:, :, :, i] / std[i]

        if _X_val is not None:
            newX_test = np.ones(_X_val.shape)
            for i in range(3):
                mean[i] = np.mean(_X_val[:, :, :, i])
                std[i] = np.std(_X_val[:, :, :, i])

            for i in range(3):
                newX_val[:, :, :, i] = _X_val[:, :, :, i] - mean[i]
                newX_val[:, :, :, i] = newX_val[:, :, :, i] / std[i]

        print("mean after normalization: {}".format(np.mean(_X_train)))
        print("std after normalization: {}".format(np.std(_X_train)))

        return newX_train, newX_test, newX_val

    @staticmethod
    def prepareBatch(_batch_x, _batch_y, _num_diag, _num_classes, _diagnostic, _batch_adv_x=None, _batch_adv_y=None):
        if _batch_adv_x is None:
            _batch_adv_x=[]
        if _batch_adv_y is None:
            _batch_adv_y=[]
        assert (len(_batch_x) == 0, "ERR - Dataset: empty training dataset")
        assert (len(_batch_y) == 0, "ERR - Dataset: empty labeling dataset")
        assert (len(_batch_y) != len(_batch_x), "ERR - Dataset: size of datasets is not equal")

        lenght_batch = _batch_x.shape[0]
        x_batch = _batch_x.shape[1]
        y_batch = _batch_x.shape[2]
        z_batch = _batch_x.shape[3]

        if _diagnostic:
            if _batch_adv_x:
                adv_index_start = lenght_batch * (_num_diag - 1)
                adv_index_end = lenght_batch * (_num_diag - 1) + len(_batch_adv_x)
                X_train = np.zeros(shape=(adv_index_end, x_batch, y_batch, z_batch))
                Y_train = np.zeros(shape=(adv_index_end, _num_classes))
                Y_train_d = np.zeros(shape=(adv_index_end, _num_diag))

                # Adding adversary images into batch

                X_train[adv_index_start: adv_index_end, :, :, :] = copy.deepcopy(_batch_adv_x)
                Y_train[adv_index_start: adv_index_end, :] = copy.deepcopy(_batch_adv_y)
                Y_train_d[adv_index_start: adv_index_end, _num_diag] = 1.0

            else:
                X_train = np.zeros(shape=(lenght_batch * _num_diag, x_batch, y_batch, z_batch))
                Y_train = np.zeros(shape=(lenght_batch * _num_diag, _num_classes))
                Y_train_d = np.zeros(shape=(lenght_batch * _num_diag, _num_diag))

            X_train[:lenght_batch, :, :, :] = _batch_x

            # Blackout of R,G,B channel
            R_blackout = copy.deepcopy(_batch_x)
            R_blackout[:, :, :, 0] = 0.0
            X_train[lenght_batch:lenght_batch * 2, :, :, :] = R_blackout
            G_blackout = copy.deepcopy(_batch_x)
            G_blackout[:, :, :, 1] = 0.0
            X_train[lenght_batch * 2:lenght_batch * 3, :, :, :] = G_blackout
            B_blackout = copy.deepcopy(_batch_x)
            B_blackout[:, :, :, 2] = 0.0
            X_train[lenght_batch * 3:lenght_batch * 4, :, :, :] = B_blackout

            # Adding noise to every single channel
            normal_noise = np.random.normal(loc=0.3, scale=0.1, size=(x_batch, y_batch, z_batch))
            X_train[lenght_batch * 4:lenght_batch * _num_diag, :, :, :] = copy.deepcopy(_batch_x)
            X_train[lenght_batch * 4:lenght_batch * _num_diag, :, :, :] += normal_noise

            # Concatenating labels
            for n_diag in range(_num_diag):
                Y_train[int(lenght_batch * n_diag):lenght_batch * (n_diag + 1), :] = copy.deepcopy(_batch_y)

            for n_diag in range(_num_diag):
                Y_train_d[int(lenght_batch * n_diag):lenght_batch * (n_diag + 1), n_diag] = 1.0

        else:
            X_train = np.zeros(shape=(lenght_batch, x_batch, y_batch, z_batch))
            X_train = copy.deepcopy(_batch_x)

            Y_train = np.zeros(shape=(lenght_batch, _num_classes))
            Y_train = copy.deepcopy(_batch_y)

            Y_train_d = np.zeros(shape=(lenght_batch, 1))


        assert (Y_train.shape[0] != X_train.shape[0], "ERR - Training dataset: size of dataset and labels is not equal")
        assert (Y_train_d.shape[0] != X_train.shape[0], "ERR - Diagnostic dataset: size of dataset and labels is not equal")

        return X_train, Y_train, Y_train_d

    def preprocessDataGeneral(self, _X_train, _X_test, _num_diag, _X_val=None):
        (X_train_CIFAR, Y_train_CIFAR), (X_test_CIFAR, Y_test_CIFAR) = cifar10.load_data()
        X_train = np.ndarray(shape=(_X_train.shape[0] * _num_diag, _X_train.shape[1],
                                    _X_train.shape[2], _X_train.shape[3]))  # _X_train=X_train
        X_train[:, :, :, :] = 0.0

        X_test = np.ndarray(shape=(_X_test.shape[0] * _num_diag, 32, 32, 3))

        X_test[:, :, :, :] = 0.0

        Y_train = np.ndarray(shape=(Y_train_CIFAR.shape[0] * _num_diag, 10))
        Y_test = np.ndarray(shape=(Y_test_CIFAR.shape[0] * _num_diag, 10))

        Y_train[:Y_train_CIFAR.shape[0], :] = np.copy(Y_train_CIFAR)
        Y_train[Y_train_CIFAR.shape[0]:Y_train_CIFAR.shape[0] * 2, :] = np.copy(Y_train_CIFAR)
        Y_train[Y_train_CIFAR.shape[0] * 2:Y_train_CIFAR.shape[0] * 3, :] = np.copy(Y_train_CIFAR)
        Y_train[Y_train_CIFAR.shape[0] * 3:Y_train_CIFAR.shape[0] * 4, :] = np.copy(Y_train_CIFAR)

        Y_test[:Y_test_CIFAR.shape[0], :] = np.copy(Y_test_CIFAR)
        Y_test[Y_test_CIFAR.shape[0]:Y_test_CIFAR.shape[0] * 2, :] = np.copy(Y_test_CIFAR)
        Y_test[Y_test_CIFAR.shape[0] * 2:Y_test_CIFAR.shape[0] * 3, :] = np.copy(Y_test_CIFAR)
        Y_test[Y_test_CIFAR.shape[0] * 3:Y_test_CIFAR.shape[0] * 4, :] = np.copy(Y_test_CIFAR)

        X_train[:_X_train.shape[0], :, :, :] = np.copy(_X_train)
        R_blackout = np.copy(_X_train)
        R_blackout[:, :, :, 0] = 0.0
        X_train[_X_train.shape[0]:_X_train.shape[0] * 2, :, :, :] = R_blackout  # Blackout of R channel
        G_blackout = np.copy(_X_train)
        G_blackout[:, :, :, 1] = 0.0
        X_train[_X_train.shape[0] * 2:_X_train.shape[0] * 3, :, :, :] = G_blackout  # Blackout of G channel
        B_blackout = np.copy(_X_train)
        B_blackout[:, :, :, 2] = 0.0
        X_train[_X_train.shape[0] * 3:_X_train.shape[0] * 4, :, :, :] = B_blackout  # Blackout of B channel

        X_test[:_X_test.shape[0], :, :, :] = np.copy(_X_test)
        R_blackout = np.copy(_X_test)
        R_blackout[:, :, :, 0] = 0.0
        X_test[_X_test.shape[0]:_X_test.shape[0] * 2, :, :, :] = R_blackout
        G_blackout = np.copy(_X_test)
        G_blackout[:, :, :, 1] = 0.0
        X_test[_X_test.shape[0] * 2:_X_test.shape[0] * 3, :, :, :] = G_blackout
        B_blackout = np.copy(_X_test)
        B_blackout[:, :, :, 2] = 0.0
        X_test[_X_test.shape[0] * 3:_X_test.shape[0] * 4, :, :, :] = B_blackout

        Y_train_d = np.copy(Y_train[:, :_num_diag])
        Y_test_d = np.copy(Y_test[:, :_num_diag])

        Y_train_d[:, :] = 0.0
        Y_test_d[:, :] = 0.0

        Y_train_d[:Y_train_CIFAR.shape[0], 0] = 1.0
        Y_test_d[:Y_test_CIFAR.shape[0], 0] = 1.0

        Y_train_d[Y_train_CIFAR.shape[0]:Y_train_CIFAR.shape[0] * 2, 1] = 1.0
        Y_test_d[Y_test_CIFAR.shape[0]:Y_test_CIFAR.shape[0] * 2, 1] = 1.0

        Y_train_d[Y_train_CIFAR.shape[0] * 2:Y_train_CIFAR.shape[0] * 3, 2] = 1.0
        Y_test_d[Y_test_CIFAR.shape[0] * 2:Y_test_CIFAR.shape[0] * 3, 2] = 1.0

        Y_train_d[Y_train_CIFAR.shape[0] * 3:Y_train_CIFAR.shape[0] * 4, 3] = 1.0
        Y_test_d[Y_test_CIFAR.shape[0] * 3:Y_test_CIFAR.shape[0] * 4, 3] = 1.0

        return X_train, Y_train, X_test, Y_test

    def loadSignsDataSetFromPath(self, directory):

        if (self.labels == True):
            self.Y_train_list = []

        forbiddenString = "G1,G2,G3,G4,G5,I1,IJ4b,IP1a,IP1b,IP4a,IP9,X1,X2,X3,Z4a,Z4b,Z4c,Z4e,Z5d,Z9"
        self.listOfForbidden = forbiddenString.split(",")
        print(self.listOfForbidden)
        # Not really helpful sings:
        '''
        G1,G2,G3,G4,G5
        I1, IJ4b
        IP1a, IP1b, IP4a, IP9,
        IS - the whole- information signs with names of cities etc... not necessary - it would bring too much variability
        X1, X2, X3
        All Zs except Z3
        '''

        for root, folders, files in os.walk(directory):
            # print (len(files))
            if len(files) > self.minNumberOfImages and root != '.':
                sign = os.path.split(root)[-1]
                # creating list of signs names
                if (sign != "dataSet_mini") or (len(sign) < 1):
                    # Preventing strange classes to be part of my data-set
                    if (sign not in self.listOfForbidden) and ("IS" not in sign):
                        print(sign)
                        print(len(files))
                        self.signs.append(sign)
                        impath = os.path.join("dataSet_mini", sign)
                        self.num_classes += 1
                        print(impath)
                        for file in os.listdir(impath):
                            if file.endswith(".jpg") or file.endswith(".png"):
                                # adding into dictionary image paths
                                img_rgb = imread(os.path.join(impath, file))
                                w, h, d = img_rgb.shape
                                if 47 < w or 47 < h:
                                    setKey(self.pathes, sign, str(os.path.join(impath, file)))
                                    self.dataSetLen += 1
                                    if (self.labels == True):
                                        self.Y_train_list.append(self.num_classes)
                                else:
                                    print("Too small resolution: {}".format(file))
                                    os.remove(os.path.join(impath, file))

        print("Length of dataset is: {}".format(self.dataSetLen))

        print("Number of classes: {}".format(self.num_classes))

        self.training_shape = (self.dataSetLen, self.img_rows, self.img_cols, self.channels)

        print("Training shape: {}".format(self.training_shape))

        self.X_train_pos = np.zeros(shape=self.training_shape, dtype=np.uint8)
        self.X_train_neg = np.zeros(shape=self.training_shape, dtype=np.uint8)

        self.X_test_pos = np.zeros(shape=self.training_shape, dtype=np.uint8)
        self.X_test_neg = np.zeros(shape=self.training_shape, dtype=np.uint8)

        self.Y_train = np.zeros(shape=self.dataSetLen, dtype=np.uint8)
        self.Y_test = np.zeros(shape=self.dataSetLen, dtype=np.uint8)

    def loadImages(self):
        allIndexPos = 0
        # defining ration between positive and negative samples
        for sign, empty in self.pathes.items():
            for imgPath in self.pathes[sign]:
                if self.labels is True:
                    print(self.Y_train_list[allIndexPos])
                    self.Y_train[allIndexPos] = self.Y_train_list[allIndexPos]

                if self.channels == 1:
                    img_rgb = misc.imread(imgPath)
                    img_grey = imread(imgPath)
                    img_grey = misc.imresize(img_grey, size=(self.img_rows, self.img_cols, self.channels),
                                             interp='bilinear', mode=None)
                    print(img_grey.dtype)
                    print(img_grey.shape)
                else:
                    img_rgb = imread(imgPath)
                    img_rgb = misc.imresize(img_rgb, size=(self.img_rows, self.img_cols, self.channels),
                                            interp='bilinear', mode=None)
                    img_grey = imread(imgPath)
                if self.channels == 1:
                    self.X_train_pos[allIndexPos, :, :, 0] = ((img_grey.astype(np.float32) / 255) - 0.5) * 2

                else:
                    self.X_train_pos[allIndexPos, :, :, :] = img_rgb.astype(np.float32)
                allIndexPos += 1

        return allIndexPos, self.num_classes

    def loadImages_pairs(self):

        allIndexPos = 0
        allIndexNeg = 0
        # defining ration between positive and negative samples
        img_con = np.zeros(shape=(28, 28, 2), dtype=np.uint8)
        for number in range(10):
            for index in range(len(self.pathes[number])):
                # inp_gen = "gen_img/%d/%d_%d.png" % (number, number ,index)
                inp_ori = "train_img/%d/%d_%d.png" % (number, number, index)
                # print ("Generated number {}_{}, was loaded".format(number, index))
                img_gen = misc.imread(self.pathes[number][index])
                img_ori = misc.imread(inp_ori)
                img_con[:, :, 0] = img_gen
                img_con[:, :, 1] = img_ori
                self.X_train_pos[allIndexPos, :, :, :] = img_con
                allIndexPos += 1

            # creating negative samples
            for index in range(len(self.pathes[number])):
                # generating random number to create negative sample
                randNum = np.random.randint(10, size=1)
                if randNum == number:
                    if randNum == 9:
                        randNum -= 1
                    else:
                        randNum += 1
                inp_ori = "train_img/%d/%d_%d.png" % (randNum, randNum, index)

                img_gen = misc.imread(self.pathes[number][index])
                img_ori = misc.imread(inp_ori)

                img_con[:, :, 0] = img_gen
                img_con[:, :, 1] = img_ori

                self.X_train_neg[allIndexNeg, :, :, :] = img_con  # concatenating array together
                allIndexNeg += 1
        return len(self.X_train_pos), len(self.X_train_neg)
