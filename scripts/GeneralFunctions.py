import os
import math
import collections
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from PIL import Image
plt.rcParams.update({'figure.max_open_warning': 0})


class GeneralFunctions:

    @staticmethod
    def setKey(dictionary, key, value):
            if key not in dictionary:
                dictionary[key] = [value]
            elif type(dictionary[key]) == list:
                dictionary[key].append(value)
            else:
                dictionary[key] = [dictionary[key], value]

    def filter_weak_samples(self, _input_path, _model, _output_path, _ground_truth, _labels, _threshold):
        information_list = []
        valid_images = [".jpg", ".png"]

        for all_images in os.listdir(_input_path):
            # Verify valid extension
            ext = os.path.splitext(all_images)[1]
            if ext.lower() not in valid_images:
                continue

            np_img, or_img = self.load_image(os.path.join(_input_path, all_images), 224)
            prediction = _model.predict(np.expand_dims(np_img, axis=0))
            print(_ground_truth)
            print(int(np.argmax(prediction[0])))
            print(prediction[0][np.argmax(prediction[0])])
            if _ground_truth is not int(np.argmax(prediction[0])):
                print("Not classified as groundtruth class")
                or_img.save(os.path.join(_output_path, all_images))
                information_list.append([all_images,
                                        np.argmax(prediction[0]),
                                        _labels[np.argmax(prediction[0])],
                                        prediction[0][np.argmax(prediction[0])]])
            else:
                if prediction[0][np.argmax(prediction[0])] < _threshold:
                    print("Groundtruth class classiffied with weak confidence")

                    or_img.save(os.path.join(_output_path, all_images))
                    information_list.append([all_images,
                                            np.argmax(prediction[0]),
                                            _labels[np.argmax(prediction[0])],
                                            prediction[0][np.argmax(prediction[0])]])
                else:
                    print("Strong and correct classification")
                    print(prediction)

        file_path = os.path.join(_output_path, "confidences_overview.csv")
        np.savetxt(file_path, information_list, delimiter=",", fmt='%s')

    # Inverse of the preprocessing and plot the image
    @staticmethod
    def plot_img(x):
        """
        x is a BGR image with shape (? ,224, 224, 3)
        """
        t = np.zeros_like(x[0])
        t[:, :, 0] = x[0][:, :, 2]
        t[:, :, 1] = x[0][:, :, 1]
        t[:, :, 2] = x[0][:, :, 0]
        plt.imshow(t)
        plt.grid('off')
        plt.axis('off')
        plt.show()

    @staticmethod
    def plot_x_y(title, x1, x1_label, x2, x2_label, x3, x3_label, x4, x4_label, x5, x5_label, x6, x6_label):
        """

        """
        fig, ax = plt.subplots(1, 1)

        # Example data
        labels = (x1_label, x2_label, x3_label, x4_label, x5_label, x6_label)
        y_pos = np.arange(len(labels))
        performance = (x1, x2, x3, x4, x5, x6)
        error = np.random.rand(len(labels))

        ax.barh(y_pos, performance, xerr=error, align='center')
        ax.set_yticks(y_pos)
        ax.set_yticklabels(labels)
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_ylabel('Test Scores')
        ax.set_xlabel('Performance')
        ax.set_title(title)

        plt.show()

    @staticmethod
    def update(_r, _temp_y, _temp_x):
        _r.data_source.stream({'x': [_temp_x], 'y': [_temp_y]})


    @staticmethod
    def update_plot(_y, _temp_y, _x, _plt):
        _y.append(_temp_y)
        _plt.scatter(_x, _temp_y)
        _plt.draw()

    @staticmethod
    def openTxtAsDict(path):
        d = {}
        with open(path) as f:
            for line in f:
               (key, val) = line.split()
               d[int(key)] = val
        return d

    @staticmethod
    def openTxtAsList(path):
        l = []
        with open(path) as f:
            for line in f:
               (key, output_class, rest) = line.split("'", 2)
               l.append(output_class)
        return l

    @staticmethod
    def openTxtAsNumpy(path):
        lst = []
        with open(path) as f:
            for line in f:
               val = line.rstrip().split(",")
               lst.append(np.array(val, dtype=np.float32))
        return lst


    @staticmethod
    def saveNumpyArray(_np_data, _path, _name, _index=None):

        # add a 'best fit' line
        weights_path = os.path.join(_path, _name)

        if not os.path.exists(weights_path):
            os.makedirs(weights_path)
        if _index:
            np.save(os.path.join(weights_path, "%04d_" % _index) + _name, _np_data)
        else:
            np.save(weights_path, _np_data)

    @staticmethod
    def plotHistogram_confidence(_np_data, _bar_path, _name, _index, _labels, _title):

        conf_path = os.path.join(_bar_path, _name)
        if not os.path.exists(conf_path):
            os.makedirs(conf_path)

        x = np.arange(len(_labels))
        width = 0.8

        fig, ax = plt.subplots()
        bar_ref = ax.bar(x, _np_data, width, color='r')
        ax.set_ylabel('Confidences')
        ax.set_title(_title)
        ax.set_xticks(x)
        ax.set_xticklabels(_labels)
        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.yaxis.set_major_formatter(FormatStrFormatter('%.4f'))

        def autolabel(rects):
            """Attach a text label above each bar in *rects*, displaying its height."""
            for rect in rects:
                height = rect.get_height()
                ax.annotate('{:.4f}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height),
                            xytext=(0, 3),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center', va='bottom')

        autolabel(bar_ref)
        fig.tight_layout()

        plt.savefig(os.path.join( conf_path, "%04d_histogram.png" % _index), dpi=150)


    @staticmethod
    def plotHistogram_weights(_np_data, _hist_path, _name, _index, _title):
        num_bins = 50
        fig, (ax_R, ax_G, ax_B) = plt.subplots(3,1)

        mean_R = np.mean(_np_data[:, :, 0])
        std_R = np.std(_np_data[:, :, 0])
        mean_G = np.mean(_np_data[:, :, 1])
        std_G = np.std(_np_data[:, :, 1])
        mean_B = np.mean(_np_data[:, :, 2])
        std_B = np.std(_np_data[:, :, 2])

        # add a 'best fit' line
        hist_path = os.path.join(_hist_path, _name)
        if not os.path.exists(hist_path):
            os.makedirs(hist_path)

        ax_R.set_ylabel('Probability density')
        ax_R.set_title(r'{} with $\mu={:.4f}$, $\sigma={:.4f}$'.format(_title, mean_R, std_R))

        ax_G.set_xlabel('Weight')
        ax_G.set_ylabel('Probability density')
        ax_G.set_title(r'{} with $\mu={:.4f}$, $\sigma={:.4f}$'.format(_title, mean_G, std_G))

        ax_B.set_xlabel('Weight')
        ax_B.set_ylabel('Probability density')
        ax_B.set_title(r'{} with $\mu={:.4f}$, $\sigma={:.4f}$'.format(_title, mean_B, std_B))

        # Tweak spacing to prevent clipping of ylabel
        fig.tight_layout()
        fig.savefig(os.path.join(hist_path, "%d_histogram.png" % _index), dpi=150)

    @staticmethod
    def load_numpy_array(infilename):
        image = np.load(infilename)
        return image

    @staticmethod
    def load_image(infilename, widht=224, height=224, normalize=True):
        epsilon = 1.e-9
        img_pil = Image.open(infilename)
        img_pil.load()
        newsize=(widht,height)
        img_re = img_pil.resize(newsize)
        data_np = np.ndarray(shape=(1,widht,height,3), dtype=float)
        resize_np = np.asarray(img_re, dtype='float32')
        if len(resize_np.shape) == 2:
            data_np[0, :, :, 0] = resize_np
            data_np[0, :, :, 1] = resize_np
            data_np[0, :, :, 2] = resize_np
        else:
            if resize_np.shape[2] == 4:
                data_np[0, :, :, :] = resize_np[:, :, :3]
            else:
                data_np[0, :, :, :] = resize_np

        if normalize:
            data_np /= 255.

        new_test_img = np.ones(data_np.shape)

        mean=[0, 0, 0]
        std=[0, 0, 0]
        for i in range(3):
            mean[i] = np.mean(data_np[:, :, i])
            std[i] = np.std(data_np[:, :, i])

        for i in range(3):
            new_test_img[:, :, i] = data_np[:, :, i] - mean[i]
            new_test_img[:, :, i] = new_test_img[:, :, i] / (std[i] + epsilon)

        return data_np, img_pil  # * (1.0 / new_test_img.max()))

    @staticmethod
    def plotWeightsHistogram(x, _ax, _bin, _canvas):
        mean = np.mean(x)
        std = np.std(x)
        _ax.clear()
        n, bins, patches = _ax.hist(
            x, _bin, density=1, facecolor="green", alpha=0.75)
        y = ((1 / (np.sqrt(2 * np.pi) * std)) * np.exp(-0.5 * (1 / std * (bins - mean))**2))
        _ax.plot(bins, y, '--')
        _ax.set_title(r'$\mu={:.4f}$, $\sigma={:.4f}$'.format(mean, std))
        _ax.grid(True)
        _canvas.draw()

    @staticmethod
    def joy_plots(stat_dict, _class, _model, _path, _n_worst=20):
        layers_dict = {}
        images = list(stat_dict.keys())

        layers = stat_dict[images[0]][0]
        for layer in layers.keys():
            indices_list = layers[layer]
            indices_dict = {}
            for index_dict in indices_list:
                for index_key in index_dict.keys():
                    GeneralFunctions.setKey(indices_dict, index_key, 0.0)
            GeneralFunctions.setKey(layers_dict, layer, indices_dict)

        for image in stat_dict.keys():
            layers = stat_dict[image][0]
            # print(layers)
            for layer in layers.keys():
                indices_list = layers[layer]
                indices_dict = {}
                for index_dict in indices_list:
                    for index_key in index_dict.keys():
                        layers_dict[layer][0][index_key].append(float(index_dict[index_key]))

        layers_indices_criticality_dict=collections.defaultdict(list)

        for layer in layers_dict.keys():
            indices_dict = layers_dict[layer][0]

            k = len(indices_dict.keys())
            x_axis = len(images)

            x = list()

            # Create the data
            labels_indices = list()
            labels = list()
            labels_temp = list()
            number_of_labels = 12
            if k > number_of_labels:
                label_step = math.floor(k / (number_of_labels-1))
            else:
                label_step = 2
            for index in range(0,len(indices_dict.keys()),label_step):
                labels_indices.append(index)

            for i, index in enumerate(indices_dict.keys()):
                x += indices_dict[index][1:]
                mean = np.mean(indices_dict[index][1:])
                std = np.std(indices_dict[index][1:])
                layers_indices_criticality_dict[layer].append({index:[mean,std]})
                labels.append(i)
                if i in labels_indices:
                    labels_temp.append(i)
                else:
                    labels_temp.append(None)

            g = np.tile(labels, x_axis)

        cri_tau = 0.5
        fig_dir = os.path.join(_path, _model, _class, "models_layers_histograms")
        if not os.path.exists(fig_dir):
            os.makedirs(fig_dir)

        list_of_layers = list()
        list_of_layers_means = list()
        list_of_layers_stds = list()
        weak_hypothesis = list()
        worst_neurons = collections.defaultdict(list)

        plt.rcParams.update({'font.size': 6})
        error_kw = {'elinewidth': 0.5, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'black'}
        fig, ax = plt.subplots(figsize=(7, 3))

        for layer in layers_indices_criticality_dict.keys():
            list_of_layers.append(layer)
            indices_dict = layers_indices_criticality_dict[layer]
            list_of_neurons = list()
            list_of_stds = list()
            list_of_means = list()
            for neurons_dicts in indices_dict:
                for neurons_index, neurons_values in neurons_dicts.items():
                    list_of_neurons.append(str(neurons_index))
                    list_of_means.append(neurons_values[0])
                    list_of_stds.append(neurons_values[1])

            indexes = np.argsort(list_of_means)
            if len(list_of_means) < _n_worst:
                top_x = indexes[-len(list_of_means):]
            else:
                top_x = indexes[-_n_worst:]
            top_x_means = [float(list_of_means[index]) for index in top_x]
            top_x_stds = [float(list_of_stds[index]) for index in top_x]

            colors = list()
            for index in range(len(top_x_means)):
                criticality = top_x_means[index] + top_x_stds[index]
                worst_neurons[layer].append({top_x[index]: top_x_means[index]})

                if criticality > cri_tau:
                    colors.append('red')
                    weak_hypothesis.append(layer)
                elif criticality > cri_tau / 2:
                    colors.append('orange')
                elif criticality > cri_tau / 10:
                    colors.append('yellow')
                elif criticality < 0.0:
                    colors.append('blue')
                else:
                    colors.append('green')

            ax.clear()
            y_pos = np.arange(len(top_x))
            ax.barh(y_pos, top_x_means, xerr=top_x_stds, alpha=0.5, align='center', color=colors, error_kw=error_kw)
            ax.invert_yaxis()  # labels read top-to-bottom
            ax.set_yticks(y_pos)
            ax.set_yticklabels(top_x)
            ax.set_xlabel('Criticality')
            ax.set_ylabel('Indices')
            fig.tight_layout()
            fig.savefig(os.path.join(fig_dir, _model + "_" + layer + "_criticality_result.png"))

            list_of_layers_means.append(np.mean(list_of_means))
            list_of_layers_stds.append(np.mean(list_of_stds))

        colors = list()
        for each_layer in list_of_layers:
            if each_layer in weak_hypothesis:
                colors.append('red')
            else:
                colors.append('green')
        ax.clear()
        ax.bar(list_of_layers, list_of_layers_means, yerr=list_of_layers_stds, error_kw=error_kw, alpha=0.5, color=colors)
        if len(list_of_layers) < 20:
            plt.rcParams.update({'font.size': 10})
            labels = ax.get_xticklabels()
            plt.setp(labels, rotation=45, horizontalalignment='right', fontsize=10)
        else:
            plt.rcParams.update({'font.size': 8})
            ax.xaxis.set_ticklabels([]) # hiding all labels

        ax.set_ylabel('Mean of normalized criticality')
        fig.tight_layout()
        fig.savefig(os.path.join(fig_dir, _model + "_" + "_criticality_result.pdf"))

        return worst_neurons

    @staticmethod
    def plot_histogram_with_err(fig_dir, _model, _class, _n_worst, list_of_worst_neurons, list_of_accuracy, list_of_accuracy_stds):
        error_kw = { 'elinewidth': 0.5, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'black'}
        fig, ax = plt.subplots(figsize=(7, 3))

        bars_list = ax.bar(list_of_worst_neurons, list_of_accuracy, yerr=list_of_accuracy_stds, error_kw=error_kw, alpha=0.5)
        if _n_worst < 21:
            plt.rcParams.update({'font.size': 10})
            labels = ax.get_xticklabels()
            plt.setp(labels, rotation=45, horizontalalignment='right', fontsize=8)
        else:
            plt.rcParams.update({'font.size': 8})
            ax.xaxis.set_ticklabels([])  # hiding all labels
        bars_list[0].set_color("g")  # setting first bar as green - no masking
        ax.set_ylabel('Accuracy')
        fig.tight_layout()
        fig.savefig(os.path.join(fig_dir, _model + "_" + _class + "_" + str(_n_worst) + "_accuracy_result.pdf"))

    @staticmethod
    def plot_CDP_dependently_masking_results(_path, _statistics_dict, _or_conf, _class, _image_name, _model_name, _number_of_neurons):

        # statistics_dict[every_layer].append({max_response_filter_index: or_conf})
        layers_names = list()
        texts = list()
        values = list()

        for index, each_layer in enumerate(tqdm(_statistics_dict.keys())):
            layers_names.append(each_layer)
            texts.append(list(_statistics_dict[each_layer][0].keys())[0])
            values.append(list(_statistics_dict[each_layer][0].values())[0])

        # differences = np.diff(np.asarray(values))
        differences = np.gradient(np.asarray(values), 2)
        np.insert(differences, 0, abs(_or_conf - values[0]) / 2.0)
        # print(differences)
        # print(differences.shape)

        plt.rcParams.update({'font.size': 10})
        fig, ax = plt.subplots()

        ax.bar(layers_names, np.asarray(values), alpha=0.5)

        for index, i in enumerate(ax.patches):
            # get_x pulls left or right; get_height pushes up or down
            ax.text(i.get_x() + .12, i.get_height() - 3,
                    str(format(values[index], '.3f')), fontsize=10,
                    color='white')

        ax.set_ylabel('Confidence gradient')
        ax.set_xlabel('Layers\' names')
        fig_dir = os.path.join(_path, _model_name, _image_name, "criticality")
        if not os.path.exists(fig_dir):
            os.makedirs(fig_dir)
        fig.savefig(os.path.join(fig_dir + "Layers_dependently_masking_confidence_result_" +
                                 _class + str(_number_of_neurons) + ".png"))

    @staticmethod
    def plot_layers_responses_results(_path, _image_name, _model_name, _class, _sum_responses_dict):
        fig, ax = plt.subplots()

        for layer in _sum_responses_dict.keys():
            fm_sum_list = _sum_responses_dict[layer][0]
            fm_sum_np = np.asarray(fm_sum_list)
            norm_fm_sum_np = fm_sum_np/np.max(fm_sum_np)

            ''' --------------------------------------- '''
            ''' Plotting histogram of fm sums '''
            ''' --------------------------------------- '''
            # clear the previouse axis
            ax.clear()

            x_pos = np.arange(len(fm_sum_list))
            ax.barh(x_pos, norm_fm_sum_np, alpha=0.5,  align='center')
            ax.invert_yaxis()  # labels read top-to-bottom
            ax.set_xlabel('Neuron\'s response')
            ax.set_ylabel('Weights\' indices')
            ax.set_title("Neurons\' reponses for class {}".format(_class))

            fig_dir = os.path.join(_path, _model_name, _class, _image_name, "responses_graph")
            if not os.path.exists(fig_dir):
                os.makedirs(fig_dir)
            fig.savefig(os.path.join(fig_dir, layer + "_graph.png"))

    @staticmethod
    def plot_CDP_results(_path, _cri_stat_dict, _image_name, _model_name, _class, _dict_of_weights,
                         _cri_tau):

        top_x_neurons = "all"
        number_of_weights = list()
        list_of_layers = [layer_names for layer_names in _cri_stat_dict.keys()]

        # gathering all number of weight which will be used further to normalize the criticality
        for each_layer in _dict_of_weights.keys():
            if each_layer in _cri_stat_dict.keys():
                weights = _dict_of_weights[each_layer][0]
                if len(weights.shape) == 4:  # to filter only depth separable and conv layers
                    if weights.shape[3] == 1:
                        number_of_weights.append(weights.shape[2])
                    else:
                        number_of_weights.append(weights.shape[3])

        fig_dir = os.path.join(_path, _model_name, _class, _image_name, "criticality")
        if not os.path.exists(fig_dir):
            os.makedirs(fig_dir)

        layer_criticality = list()
        weak_hypothesis = list()
        fig, ax = plt.subplots()

        for index, each_layer in enumerate(tqdm(_cri_stat_dict.keys())):

            values = [list(criticality_dict.values())[0] for criticality_dict in
                      _cri_stat_dict[each_layer]]

            if top_x_neurons is "all":
                top_x_values = [float(value) for value in values]
                plt.rcParams.update({'font.size': 10})

            else:
                indexes = np.argsort(values)
                top_x = indexes[-top_x_neurons:]
                top_x_values = [float(values[index]) for index in top_x]
                plt.rcParams.update({'font.size': 12})

            # criticality pro layer
            only_critical_neurons = [float(criticality_value) for criticality_value in top_x_values if
                                     float(criticality_value) > 0.0]
            layer_criticality.append(np.mean(only_critical_neurons))

            x_pos = np.arange(len(top_x_values))
            colors = list()

            for values in top_x_values:
                if values > _cri_tau:
                    colors.append('red')
                    weak_hypothesis.append(each_layer)
                elif values > _cri_tau / 2:
                    colors.append('orange')
                elif values > _cri_tau / 10:
                    colors.append('yellow')
                elif values < 0.0:
                    colors.append('blue')
                else:
                    colors.append('green')
            # clear the previous axis
            ax.clear()
            ax.barh(x_pos, top_x_values, alpha=0.5, color=colors, align='center')
            ax.invert_yaxis()  # labels read top-to-bottom
            ax.set_xlabel('Criticality')
            ax.set_ylabel('Indices')

            fig.tight_layout()
            fig.savefig(os.path.join(fig_dir, each_layer + "_CNA_result.png"))

            ''' --------------------------------------- '''
            ''' Plotting histogram of L2-norms '''
            ''' --------------------------------------- '''
            # clear the previous axis
            ax.clear()

            mean = np.mean(top_x_values)
            std = np.std(top_x_values)
            entropy = False
            if entropy:
                newX_top_x_values = top_x_values - mean
                newX_top_x_values = newX_top_x_values / std
                layers_entropy = entropy(newX_top_x_values, base=2)
                ax.text(0, .5, layers_entropy)

            ax.set_ylabel("Density")
            fig.savefig(os.path.join(fig_dir, each_layer + "_histogram.png"))
            GeneralFunctions.plot_models_layers_criticality(list_of_layers, layer_criticality, weak_hypothesis,
                                                            fig_dir, _model_name + each_layer, _class)

    @staticmethod
    def plot_models_layers_criticality(_list_of_layers, layer_criticality, weak_hypothesis, fig_dir, _model_name,
                                       _class):
        colors = list()
        for each_layer in _list_of_layers:
            if each_layer in weak_hypothesis:
                colors.append('red')
            else:
                colors.append('green')

        plt.rcParams.update({'font.size': 10})
        fig, ax = plt.subplots(figsize=(6, 3))

        ax.bar(_list_of_layers, layer_criticality, alpha=0.5, color=colors)
        labels = ax.get_xticklabels()
        plt.setp(labels, rotation=45, horizontalalignment='right')
        plt.rcParams.update({'font.size': 12})
        ax.set_title("Layers\' normalized criticality for model: {}, for class: {}".format(_model_name, _class))
        ax.set_ylabel('Mean of normalized criticality')
        ax.set_xlabel('Layers\' names')
        # Tweak spacing to prevent clipping of tick-labels
        fig.tight_layout()
        fig.savefig(os.path.join(fig_dir, _model_name + "_" + "_criticality_result.pdf"))
