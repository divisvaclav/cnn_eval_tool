import numpy as np


from PySide6.QtCore import Qt, Signal, QTimer
from PySide6.QtWidgets import QWidget

from sfml import sf


class QSFMLCanvas(QWidget):

    visualization_running_signal = Signal(bool)

    def __init__(self, parent, position, size, additional_info, stream_source):
        QWidget.__init__(self, parent)
        self.initialized = False
        self.onInit()
        w = size.width()
        h = size.height()
        self.on_update_get_outputs_pause = True
        self._HandledWindow = sf.HandledWindow()
        self._HandledWindow.view.size = (w, h)
        self.__dict__['draw'] = self._HandledWindow.draw
        self.__dict__['clear'] = self._HandledWindow.clear
        self.__dict__['view'] = self._HandledWindow.view
        self.__dict__['display'] = self._HandledWindow.display

        # setup some states to allow direct rendering into the widget
        self.setAttribute(Qt.WA_PaintOnScreen)
        self.setAttribute(Qt.WA_OpaquePaintEvent)
        self.setAttribute(Qt.WA_NoSystemBackground)

        # set strong focus to enable keyboard events to be received
        self.setFocusPolicy(Qt.StrongFocus)

        # setup the widget geometry
        self.move(position)
        self.resize(size)

        self.additional_info = additional_info
        self.stream_source = stream_source
        self.index_testset = 0

        self.R, self.G, self.B, self.N, self.A = False, False, False, False, False
        self.D, self.prev_D = False, False

        # setup the timer
        self.frameTime = 0
        self.timer = QTimer()
        self.timer.setInterval(self.frameTime)
        self.onUpdateGetOutputs = True
        self.counter = 0
        self.inference_time = list()

    def resizeCanvas(self, position, size):
        self._HandledWindow.view.size = (1920, 1080)
        self.move(position)
        self.resize(size)

    def mouseDoubleClickEvent(self, event):  # event is removed from arg
        x_c, y_c = self._HandledWindow.view.center
        print("mouse double click")
        print(x_c, y_c)
        x_m = event.get("x")
        y_m = event.get("y")

        if x_c > 0:
            self._HandledWindow.view.zoom(1.05)
            self._HandledWindow.view.move((x_c-x_m) / 8.0, (y_c-y_m) / 8.0)

        else:
            self._HandledWindow.view.zoom(0.95)
            self._HandledWindow.view.move((x_m-x_c) / 8.0, (y_m-y_c) / 8.0)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            if self.timer.isActive():
                if self.stream_source:
                    self.on_update_get_outputs_pause = not(self.on_update_get_outputs_pause)
                    self.visualization_running_signal.emit(self.on_update_get_outputs_pause)
                else:
                    self.index_testset += 1

            fps_mean = np.mean(np.asarray(self.inference_time))
            print("Average fps: {}".format(fps_mean))
            self.inference_time = list()

        if event.key() == Qt.Key_Minus:
            self._HandledWindow.view.zoom(1.05)

        elif event.key() == Qt.Key_Plus:
            self._HandledWindow.view.zoom(0.95)

        elif event.key() == Qt.Key_Left:
            self._HandledWindow.view.move(10, 0)

        elif event.key() == Qt.Key_Right:
            self._HandledWindow.view.move(-10, 0)

        elif event.key() == Qt.Key_Up:
            self._HandledWindow.view.move(0, 10)

        elif event.key() == Qt.Key_Down:
            self._HandledWindow.view.move(0, -10)

        elif event.key() == Qt.Key_Escape:

            print("Pressed escape")

        elif event.key() == Qt.Key_R:
            print("RED: {}".format(self.R))
            self.R = not(self.R)
        elif event.key() == Qt.Key_G:
            print("GREEN: {}".format(self.G))
            self.G = not(self.G)
        elif event.key() == Qt.Key_B:
            print("BLUE: {}".format(self.B))
            self.B = not(self.B)
        elif event.key() == Qt.Key_N:
            print("NOISE: {}".format(self.N))
            self.N = not(self.N)
        elif event.key() == Qt.Key_A:
            print("ADVERSARY: {}".format(self.A))
            self.A = not(self.A)
        elif event.key() == Qt.Key_D:
            print("Deleting Filter: {}".format(self.D))
            self.D = not(self.D)


    def sizeHint(self):
        return self.size()

    def paintEngine(self):
        # let the derived class do its specific stuff
        if not(self.on_update_get_outputs_pause):
            running = self.onUpdateGetOutputs
            if running == False:
                self.on_update_get_outputs_pause = True
                self.visualization_running_signal.emit(False)

        self.onUpdatePaint()

        # display on screen
        self.display()


    def showEvent(self, event):
        if not self.initialized:
            # under X11, we need to flush the commands sent to the server
            # to ensure that SFML will get an updated view of the windows
            # create the SFML window with the widget handle

            self._HandledWindow.create(self.winId())

            # let the derived class do its specific stuff
            self.onInit()

            # setup the timer to trigger a refresh at specified framerate
            self.timer.timeout.connect(self.repaint)
            self.timer.start()
            self.initialized = True

    def paintEvent(self, event):
        return None

    def onUpdatePaint(self):
        self.clear(sf.Color.WHITE)

        # Visualizing normal convolutional layer
        for key in self.dict_of_convs_sprite.keys():
            for each_fmap in self.dict_of_convs_sprite[key]:
                self.draw(each_fmap)
        # Visualizing dense layer
        for key in self.dict_of_denses.keys():
            for each_neuron in self.dict_of_denses[key]:
                self.draw(each_neuron)

        for each_layer_label in self.list_of_layer_labels:
            self.draw(each_layer_label)

        for each_confidence_label in self.list_of_confidences:
            self.draw(each_confidence_label)

        if self.diag_layers is not None:
            # Visualizing diag convolutional layer
            for key in self.dict_of_convs_sprite_diag.keys():
                for each_fmap_diag in self.dict_of_convs_sprite_diag[key]:
                    self.draw(each_fmap_diag)

            for key in self.dict_of_denses_diag.keys():
                for each_neuron_diag in self.dict_of_denses_diag[key]:
                    self.draw(each_neuron_diag)

            for each_confidence_label_diag in self.list_of_confidences_diag:
                self.draw(each_confidence_label_diag)

        if self.additional_info:
            for each_graph_sprite in self.list_of_graphs_sprite:
                self.draw(each_graph_sprite)

        if self.grad_cam:
            self.draw(self.sf_input_img_sprite)
