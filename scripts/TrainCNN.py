
import numpy as np
import os
import tensorflow as tf

import matplotlib.pyplot as plt
import keras
from tqdm import tqdm
from contextlib import redirect_stdout
import re
import tempfile
from tensorflow.python import keras


from keras.layers import Dense, Concatenate
from keras.layers import Activation, ZeroPadding2D, GlobalAveragePooling2D

from keras.layers.convolutional import Conv2D, DepthwiseConv2D
from keras.models import Sequential, Model
from tensorflow.keras.optimizers import Adam
from keras.engine.input_layer import InputLayer
from keras.applications import mobilenet_v2
from keras.utils import multi_gpu_model
from scripts.CNNWrapper import CNNWrapper

import tensorflow_model_optimization as tfmot
import DataSets as DataSets
import GeneralFunctions as GeneralFunctions


#config = tf.ConfigProto()
#config.gpu_options.allow_growth = True
#config.gpu_options.per_process_gpu_memory_fraction = 0.9
#keras.backend.tensorflow_backend.set_session(tf.Session(config=config))


# In[2]:
class MobileNet_V2:

    def __init__(self):

        self.dataSet = DataSets.DataSet()
        self.genFun = GeneralFunctions.GeneralFunctions()
        # In[5]:
        self.batchSize = 10                                     #-- Training Batch Size
        self.epochs = 20                                        #-- Number of epochs for training
        self.learningRate = 1e-4                                #-- Learning rate for the network
        self.dropout = 0.2

        self.img_rows = 0
        self.img_cols = 0
        self.channels = 0

        self.class_list = self.genFun.openTxtAsList(os.path.join("data","dataset","pedestrian_clsidx_to_labels.txt"))
        self.num_classes = len(self.class_list)  # Number of classes
        self.diag_list = self.genFun.openTxtAsList(os.path.join("data","dataset","diagnoses_clsidx_to_labels.txt"))
        self.num_diag = len(self.diag_list)  # [normal, R, G, B, noise, adversary, synthetised]

        self.img_rows, self.img_cols, self.channels = 224, 224, 3

        self.diag = True
        self.transfer_learning = True
        self.verbose = False
        self.openM = CNNWrapper
        self.model_path = ''
        self.weights_path = ''

        ''' Test 1 '''
        # Conv1_relu (Conv2D)                  (None, 112, 112, 32) 864
        # block_2_expand_relu (Conv2D)         (None, 56, 56, 144)  3456
        ''' Test 2 '''
        #block_4_expand_relu (Conv2D)         (None, 28, 28, 192)  6144
        #block_8_expand_relu (Conv2D)         (None, 14, 14, 384)  24576

        ''' Test 3 '''
        #block_10_expand_relu (Conv2D)        (None, 14, 14, 384)  24576
        #block_16_expand_relu (Conv2D)        (None, 7, 7, 960)    153600

        ''' Test 4 '''
        #block_1_expand_relu (Conv2D)         (None, 112, 112, 96) 1536
        #block_14_expand_relu (Conv2D)        (None, 7, 7, 960)    153600
        if self.diag == True:
            self.diag_layers_list = [["Conv1_relu","block_1_depthwise_relu", 4, 3, 32, 144],
            ["block_4_expand_relu","block_8_expand_relu", 2, 1, 192, 384],
            ["block_10_expand_relu","block_16_expand_relu", 1, 0, 384, 960],
            ["Conv1_relu","block_16_expand_relu", 4, 0, 32, 960]]
        else:
            self.diag_layers_list = ["No diag"]

    def build_finetune_model(self, _diag_layer_attempts):

        if self.transfer_learning:
            loaded_model = mobilenet_v2.MobileNetV2(input_shape=(224,224,3), alpha=1.0, include_top=False, weights='imagenet', input_tensor=None, pooling=None, classes=1000)

            #for layer in loaded_model.layers[:-4]:
            for layer in loaded_model.layers:
                layer.trainable = True

            for layer in loaded_model.layers:
                print(layer, layer.name, layer.trainable)

            x = loaded_model.output
            pooling = GlobalAveragePooling2D()(x)
            dense = Dense(128, activation='relu', name='dense_before_output')(pooling)

            # New softmax layer
            class_output = Dense(self.num_classes, activation='softmax', name='class_output')(dense)

        else:
            self.model_path = os.path.join("data", "trained_models","MobileNetV2", "20", "model_0.2_20.json")
            self.weights_path = os.path.join("data", "trained_models","MobileNetV2", "20", "model_0.2_20.h5")
            loaded_model = self.openM.open_custom_model(self.model_path, self.weights_path, _remove_diag = False)
            class_output = loaded_model.output

        if self.diag:
            """ Start -- diagnostic concatenate"""
            layers = [layer for layer in loaded_model.layers if not isinstance(layer, InputLayer)]          # all layer names
            names = [layer.name for layer in loaded_model.layers if not isinstance(layer, InputLayer)]          # all layer names

            x_diag = layers[names.index(_diag_layer_attempts[0])]
            output_diag_1 = self.addDiagBranch(x_diag, _diag_layer_attempts[2], 1, _diag_layer_attempts[4])

            x_diag = layers[names.index(_diag_layer_attempts[1])]
            output_diag_2 = self.addDiagBranch(x_diag, _diag_layer_attempts[3], 2, _diag_layer_attempts[5])

            concat_diag = Concatenate(axis=1)([output_diag_1, output_diag_2])

            diag_output = Dense(self.num_diag, activation='softmax', name='diag_output')(concat_diag)
            """ End -- diagnostic concatenate """

            finetune_model = Model(inputs=loaded_model.input, outputs=[diag_output, class_output])
        else:
            finetune_model = Model(inputs=loaded_model.input, outputs=class_output)

        return finetune_model

    def addDiagBranch(self, _input, _number_of_conv, _layer_index, _num_of_filters):

        layer_names = ["diag_depthconv2D_", "diag_maxpool_", "diag_conv2D_", "diag_batch_", "diag_dense_"]

        output_layer = _input.output
        filters = _num_of_filters
        """ Start -- diagnostic branch """
        for _index in range(_number_of_conv):
            zero_padding2D_d = ZeroPadding2D()(output_layer)
            depth_conv2D_d = DepthwiseConv2D(kernel_size=3, strides=(2, 2), name=layer_names[0]+ str(_layer_index) + "_" + str(_index))(zero_padding2D_d)
            activation_d = Activation('relu')(depth_conv2D_d)
            conv2D_d = Conv2D(filters, kernel_size=3, name=layer_names[2]+ str(_layer_index) + "_" + str(_index))(activation_d)
            filters = filters*(_index + 1)
            output_layer = conv2D_d

        global_pool_d = GlobalAveragePooling2D(name="diag_global_average_2D_" + str(_layer_index))(output_layer)
        ##-- diagnostic branch layer 2
        #flatten_d = Flatten()(global_pool_d)
        dense_d = Dense(64, activation='relu', name=layer_names[4] + str(_layer_index))(global_pool_d)
        #dropout_d = Dropout(self.dropout, name=layer_names[3] + str(_index))(dense_d)
        """ End -- diagnostic branch """

        return dense_d

    def visualizeTraining(self, _dirName, _acc, _loss, _index=1):

        fig, axs = plt.subplots(2, 1)

        if self.diag:
            axs[0].plot(_acc[:,0], '-', label='diagnostics')
            axs[0].plot(_acc[:,1], '-', label='classification')
        else:
            axs[0].plot(_acc, '-')
        axs[0].set_title('Accuracy and loss of mobileNetV2')
        axs[0].set_ylabel('Accuracy')


        if self.diag:
            axs[1].plot(_loss[:,0], '-', label='diagnostics')
            axs[1].plot(_loss[:,1], '-', label='classification')
            axs[1].legend(['diagnostics', 'classification'], loc="upper right")
        else:
            axs[1].plot(_loss, '-')
            axs[1].legend(['classification'], loc="upper right")
        axs[1].set_xlabel('Iterations')
        axs[1].set_ylabel('Loss')

        #plt.show()
        np.savetxt(os.path.join(_dirName, "training_acc_%d.csv" % (_index)), _acc, delimiter=',')
        np.savetxt(os.path.join(_dirName, "training_loss_%d.csv" % (_index)), _loss, delimiter=',')
        fig.savefig(os.path.join(_dirName, "training_%d.pdf" % (_index)))

    def prune(self, _model, _working_dir):

        loaded_model = keras.applications.MobileNet(input_shape=(224,224,3), alpha=1.0, include_top=False, weights='imagenet', input_tensor=None, pooling=None, classes=1000)
        #loaded_model = mobilenet_v2.MobileNetV2(weights=None)

        # https://github.com/tensorflow/model-optimization/issues/40
        # https://www.tensorflow.org/model_optimization/guide/pruning/comprehensive_guide.md#prune_whole_model_sequential_and_functional

        top_model = Sequential()
        top_model.add(GlobalAveragePooling2D())
        top_model.add(Dense(128, activation='relu', name='dense_before_output'))

        # New softmax layer
        top_model.add(Dense(self.num_classes, activation='softmax', name='class_output'))

        finetune_model = tf.keras.Model(inputs=loaded_model.input, outputs=top_model(loaded_model.output))

        training_split = 90
        dataset_path = os.path.join("data","dataset","INRIA_Person_Dataset_256")
        training_dataset, training_labels, X_test, Y_test = self.dataSet.loadImagesFromPath(dataset_path, training_split, self.class_list,
                                                                            _number_of_classes=self.num_classes,
                                                                            _resize=True, _width=224, _height=224, _channels=3)

        # Compute end step to finish pruning after 2 epochs.
        batch_size = 64
        epochs = 2
        validation_split = 0.1 # 10% of training set will be used for validation set.

        num_images = training_dataset.shape[0] * (1 - validation_split)
        end_step = np.ceil(num_images / batch_size).astype(np.int32) * epochs

        pruning_schedule = tfmot.sparsity.keras.PolynomialDecay(
                               initial_sparsity=0.0, final_sparsity=0.5,
                               begin_step=0, end_step=end_step, frequency=100)

        #layer.input_shape[-1]
        pruned_model = Sequential()
        for layer in finetune_model.layers:
            if(re.match(r"dense_\d+$", layer.name)):
                 pruned_model.add(tfmot.sparsity.keras.prune_low_magnitude(
                    layer,
                    pruning_schedule,
                    block_size=(1,1)
                 ))
            else:
                pruned_model.add(layer)

        # new_pruning_params = {'pruning_schedule': tfmot.sparsity.keras.PolynomialDecay(
        #                                         initial_sparsity=0.5,
        #                                         final_sparsity=0.80,
        #                                         begin_step=0,
        #                                         end_step=end_step,
        #                                         frequency=100)}
        #
        # pruned_model = tfmot.sparsity.keras.prune_low_magnitude(loaded_model, **new_pruning_params)

        opt = Adam()
        pruned_model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

        pruned_model.summary()

        logdir = _working_dir

        callbacks = [
          tfmot.sparsity.keras.UpdatePruningStep(),
          tfmot.sparsity.keras.PruningSummaries(log_dir=logdir),
        ]

        pruned_model.fit(training_dataset, training_labels,
                          batch_size=batch_size, epochs=epochs, validation_split=validation_split,
                          callbacks=callbacks)

        model_for_export = tfmot.sparsity.keras.strip_pruning(pruned_model)

        _, pruned_keras_file = tempfile.mkstemp('.h5')
        tf.keras.models.save_model(model_for_export, pruned_keras_file, include_optimizer=False)
        print('Saved pruned Keras model to:', pruned_keras_file)

    def train(self):
        training_dataset = os.path.join("data", "dataset", "INRIA_Person_Dataset_256")
        training_split = 80

        X_train, Y_train, X_test, Y_test = self.dataSet.loadImagesFromPath(training_dataset, training_split,
                                                                           self.class_list,
                                                                           _number_of_classes=self.num_classes,
                                                                           _resize=True, _width=224, _height=224,
                                                                           _channels=3)

        self.img_rows, self.img_cols, self.channels = X_train.shape[1], X_train.shape[2], X_train.shape[3]

        X_train_adver = []
        Y_train_adver = []
        X_test_adver = []
        Y_test_adver = []
        X_test, Y_test, Y_test_d = self.dataSet.prepareBatch(X_test, Y_test,
                                                                self.num_diag, self.num_classes,
                                                                self.diag,
                                                                X_test_adver, Y_test_adver)
        print(Y_test_d)
        if self.verbose:
            print("Length X_Training {}".format(X_train.shape))
            plt.imshow(X_train[1,:,:,:], interpolation='nearest')
            print(X_train[1,:,:,:])
            plt.show()
            print("Label trainset print:")
            print(Y_train[1])
            print("Diagnostic testset print:")
            print(Y_test_d)
        for diag_layer_attempts in self.diag_layers_list:

            y = {}
            loss = []
            acc = []
            total_loss = []
            score = []

            model = self.build_finetune_model(diag_layer_attempts)

            if self.diag:
                path = os.path.join("data", "trained_models","MobileNetV2_diag", str(self.epochs) + diag_layer_attempts[0] + diag_layer_attempts[1], "model_")
                losses = {
                	"diag_output": "categorical_crossentropy",
                	"class_output": "categorical_crossentropy",
                       }
                lossWeights = {"diag_output": 1.0, "class_output": 1.0}
                metrics={'diag_output': 'categorical_accuracy', 'class_output': 'categorical_accuracy'}

            else:
                path = os.path.join("data", "trained_models","MobileNetV2", str(self.epochs), "model_")
                losses = {
                	"class_output": "categorical_crossentropy"
                        }
                lossWeights = {"class_output": 1.0}
                metrics={'class_output': 'categorical_accuracy'}

            if not os.path.exists(path):
                os.makedirs(path)

            with open(os.path.join(path, 'model_summary.txt'), 'w') as f:
                with redirect_stdout(f):
                    model.summary()
            model.summary()

            # for all layers decrease the learning rate to 5e-5
            optimizer = Adam(learning_rate=self.learningRate, beta_1=0.9, beta_2=0.999, amsgrad=False)

            # Replicates `model` on 2 GPUs.
            # This assumes that your machine has 2 available GPUs.
            parallel_model = multi_gpu_model(model, gpus=2)

            parallel_model.compile(optimizer=optimizer,
                                   loss=losses,
                                   loss_weights=lossWeights,
                                   metrics=metrics)


            for epoch in range(1, self.epochs + 1):
                print("Epoch {}".format(epoch))

                for _ in tqdm(range(self.batchSize * self.num_diag), ascii=True, desc="Epochs"):

                    # Get a random set of  training images
                    idx_train = np.random.randint(low=0, high=X_train.shape[0],size=self.batchSize)
                    image_batch, label_batch = X_train[idx_train], Y_train[idx_train]

                    if self.diag:
                        if X_train_adver:
                            idx_adversary = np.random.randint(low=0, high=len(X_train_adver), size=self.batchSize)
                            adversary_batch, adversary_label_batch = X_train_adver[idx_adversary],\
                                                                     Y_train_adver[idx_adversary]
                        else:
                            adversary_batch = []
                            adversary_label_batch = []

                        X_train_batch, Y_train_batch, Y_train_d_batch = self.dataSet.prepareBatch(image_batch,
                                                                                                  label_batch,
                                                                                                  self.num_diag,
                                                                                                  self.num_classes,
                                                                                                  self.diag,
                                                                                                  adversary_batch,
                                                                                                  adversary_label_batch)
                        parallel_model.train_on_batch(X_train_batch, [Y_train_d_batch, Y_train_batch])
                        score = parallel_model.evaluate(X_test, [Y_test_d, Y_test], verbose=1)
                        print('Total loss: {}, diag loss: {}, diag acc: {}, task loss: {}, task acc: {}'.format(score[0], score[1], score[3], score[2], score[4]))
                        total_loss.append(score[0])
                        loss.append(np.array([score[1], score[2]]))
                        acc.append(np.array([score[3], score[4]]))

                    else:
                        parallel_model.train_on_batch(image_batch, label_batch)
                        score = parallel_model.evaluate(X_test, Y_test, verbose=1)
                        print('Test loss: {}, accuracy: {}'.format(score[0], score[1]))
                        loss.append(score[0])
                        acc.append(score[1])

            #y = model.predict(X_test)
            name_model = path + str(self.dropout) + "_" + str(self.epochs) + ".json"
            name_weights = path + str(self.dropout) + "_" + str(self.epochs) + ".h5"
            name_architecture = path + "arch_" + str(self.dropout) + "_" + str(self.epochs) + ".h5"

            model_json = model.to_json()
            with open(name_model, "w") as json_file:
                json_file.write(model_json)
                # # serialize weights to HDF5
                model.save_weights(name_weights)
                model.save(name_architecture)
                print("MobileNetV2 model was saved to disk")

            self.visualizeTraining( path, np.array(acc), np.array(loss), _index=1)



if __name__ == "__main__":
    network = MobileNet_V2()
    network.train()
