import scripts.GeneralFunctions as GeneralFunctions

import os
import json
import shutil
import math
import numpy as np
from tqdm import tqdm

import tensorflow as tf
from tensorflow import keras

from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras.applications import mobilenet_v2, resnet_v2, vgg16, xception, nasnet,\
    densenet, efficientnet, vgg19, resnet50

from tensorflow.keras.models import model_from_json


class CNNWrapper:

    def __init__(self):
        self.input_image = None
        self.input_image_np = None
        self.model = None
        self.custom_layers_model = None

        self.list_of_layers = list()
        self.list_of_layers_names = list()

        self.dict_of_layers = {}
        self.dict_of_weights = {}

        self.Functions = GeneralFunctions.GeneralFunctions()

        self.list_of_classes_labels = self.Functions.openTxtAsList(
            os.path.join("data", "dataset", "imagenet1000_clsidx_to_labels.txt"))

        self.num_classes = len(self.list_of_classes_labels)  # -- Number of classes

        self.list_of_diagnoses_labels = self.Functions.openTxtAsList(
            os.path.join("data", "dataset", "diagnoses_clsidx_to_labels.txt"))

        self.num_diag = len(self.list_of_diagnoses_labels)

        self.output_layer = "class_output"

    def calculate_accuracy(self, files_path, list_of_images, model, ground_truth_class):
        list_of_predicted_confidences = list()
        list_of_classes = [0.0] * 1000
        index = self.list_of_classes_labels.index(ground_truth_class)
        list_of_classes[index] = 1.0
        m = tf.keras.metrics

        for image_name in tqdm(list_of_images):
            self.input_image_np, _ = self.Functions.load_image(
                os.path.join(files_path, image_name), widht=224, height=224, normalize=False)
            self.preprocess_input_for_used_model(model)
            output = self.custom_layers_model.predict(self.input_image_np)[-1]
            m.update_state(np.asarray(list_of_classes), output)
            list_of_predicted_confidences.append(output[index])

        print("Models accuracy {}".format(m.result().numpy()))
        return m.result().numpy(), np.std(list_of_predicted_confidences)

    def open_custom_model(self, _model_path, _weight_path, _remove_diag=False):
        json_file = open(_model_path, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = model_from_json(loaded_model_json)

        # load weights from the model
        self.model.load_weights(_weight_path)
        print("Model was successfully loaded from disk {}".format(_weight_path))

        if _remove_diag:
            new_model = keras.Sequential()
            for layer in self.model.layers:  # go through until last layer
                if "diag" not in layer.name:
                    print(layer.name)
                    new_model.add(layer)
            new_model.summary()

        else:
            self.model.summary()
            self.dict_of_layers, self.dict_of_weights = self.get_layers_and_weights(self.model)

    def preprocess_input_for_used_model(self, model):
        if model == "VGG16":
            self.input_image_np = vgg16.preprocess_input(self.input_image_np)
        elif model == "MobileNetV2":
            self.input_image_np = mobilenet_v2.preprocess_input(self.input_image_np)
        elif model == "ResNet50V2":
            self.input_image_np = resnet_v2.preprocess_input(self.input_image_np)
        elif model == "EfficientNetB0":
            self.input_image_np = efficientnet.preprocess_input(self.input_image_np)
        elif model == "Xception":
            self.input_image_np = xception.preprocess_input(self.input_image_np)
        elif model == "NASNetLarge":
            self.input_image_np = nasnet.preprocess_input(self.input_image_np)
        elif model == "DenseNet121":
            self.input_image_np = densenet.preprocess_input(self.input_image_np)
        elif model == "EfficientNetB7":
            self.input_image_np = efficientnet.preprocess_input(self.input_image_np)
        elif model == "VGG19":
            self.input_image_np = vgg19.preprocess_input(self.input_image_np)
        elif model == "resnet50":
            self.input_image_np = vgg19.preprocess_input(self.input_image_np)
        else:
            print("Model not supported")

    def get_layers_split(self, how_many_outputs=2):
        output_layers = [layer.name.lower() for layer in self.model.layers if
                         not isinstance(layer, layers.InputLayer) and "conv2d" in layer.__class__.__name__.lower() and
                         layer.get_weights()[0].shape[3] != 1 and layer.get_weights()[0].shape[2] != 1]
        if how_many_outputs == "all":
            part = 1
        else:
            part = math.floor(len(output_layers) / how_many_outputs)

        length = len(output_layers)
        required_layers = list()
        print(length)
        print(part)
        print(output_layers)
        required_layers_temp = [output_layers[layers_index - 1] for layers_index in range(1, length, part)]
        for layer in required_layers_temp:
            required_layers.append(layer)
        last_layer = self.model.layers[-1]
        required_layers.append(last_layer.name)
        print(required_layers)
        return required_layers

    def open_DendeNet121(self, how_many_outputs):
        self.model = densenet.DenseNet121(include_top=True, weights="imagenet", input_tensor=None,
                                          input_shape=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_resnet50(self, how_many_outputs):
        self.model = resnet50.ResNet50(include_top=True, weights="imagenet", input_tensor=None,
                                          input_shape=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_vgg19(self, how_many_outputs):
        self.model = vgg19.VGG19(include_top=True, weights="imagenet", input_tensor=None,
                                          input_shape=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_EfficientNetB7(self, how_many_outputs):
        self.model = efficientnet.EfficientNetB7(include_top=True, weights="imagenet", input_tensor=None,
                                                 input_shape=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_EfficientNetB7(self, how_many_outputs):
        self.model = efficientnet.EfficientNetB7(include_top=True, weights="imagenet", input_tensor=None,
                                                 input_shape=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers



    def open_VGG16_Keras(self, how_many_outputs):
        self.model = vgg16.VGG16(include_top=True, weights="imagenet", input_tensor=None,
                                 input_shape=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_NasNetLarge_Keras(self, how_many_outputs):
        self.model = nasnet.NASNetLarge(include_top=True, weights="imagenet", input_tensor=None,
                                        input_shape=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_Xception_Keras(self, how_many_outputs):
        self.model = xception.Xception(include_top=True, weights="imagenet", input_tensor=None,
                                       input_shape=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_ResNet50V2_Keras(self, how_many_outputs):
        self.model = resnet_v2.ResNet50V2(include_top=True, weights="imagenet", input_tensor=None,
                                          input_shape=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)

        return required_layers

    def open_MobileNetV2_Keras(self, how_many_outputs):
        self.model = mobilenet_v2.MobileNetV2(input_shape=None, alpha=1.0, include_top=True, weights='imagenet',
                                              input_tensor=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)

        self.dict_of_layers, self.dict_of_weights = self.get_layers_and_weights(self.model)

        return required_layers

    def open_EfficientNetB0_Keras(self, how_many_outputs):
        self.model = efficientnet.EfficientNetB0(input_shape=None, include_top=True, weights="imagenet",
                                                 input_tensor=None, pooling=None, classes=1000)
        self.model.summary()

        required_layers = self.get_layers_split(how_many_outputs)
        print(required_layers)

        return required_layers





    def open_imageNet_validationSet_labels(self, labels_path):
        f = open(labels_path)
        real_labels = json.load(f)
        return real_labels

    def split_imageNet_validationSet_overClass(self, labels_path, validation_path, split_path):
        imagenet_classes = self.list_of_classes_labels
        f = open(labels_path)
        real_labels = json.load(f)

        # create a list of all ImageNet validation images
        files = [x for x in os.listdir(validation_path) if not x.startswith('.')]
        files.sort()

        # create dir for all classes
        for imageNet_class in imagenet_classes:
            class_path = os.path.join(split_path, imageNet_class)
            if not os.path.exists(class_path):
                os.mkdir(class_path)

        # iterate through all images and copy image to related class directory
        for label_index, image in enumerate(files):
            # list can be empty - exclude it from splitting
            if real_labels[label_index]:
                real_label = real_labels[label_index][0]
                if isinstance(real_label, str):
                    dst_class = imagenet_classes[real_label.split(",")[-1]]
                else:
                    dst_class = imagenet_classes[real_label]
                dst = os.path.join(split_path, dst_class)
                shutil.copy2(os.path.join(validation_path, image), dst)
                print("Copying {} to {}".format(image, dst))

    # create initial texture instances and save them in lists for further visualization
    def get_model_with_custom_layers(self, _list_of_layers, _custom_model=True):
        self.list_of_layers = list()
        self.list_of_layers_names = list()
        self.dict_of_layers = dict()
        self.dict_of_weights = dict()
        self.custom_layers_model = None

        for layer in self.model.layers:
            if not isinstance(layer, layers.InputLayer):
                layer_name = layer.name
                if layer_name in _list_of_layers:
                    self.list_of_layers.append(layer.output)
                    self.list_of_layers_names.append(layer_name)

            layer_name = layer.name
            if len(layer.get_weights()) > 0:
                self.dict_of_layers.update({str(layer_name): layer})
                weights = layer.get_weights()
                self.dict_of_weights.update({str(layer_name): weights})

        if _custom_model:
            # create a new instance of model, with only desired layers
            self.custom_layers_model = models.Model(inputs=self.model.inputs, outputs=self.list_of_layers)
        else:
            self.custom_layers_model = models.Model(inputs=self.model.inputs, outputs=self.list_of_layers[-1])

    @staticmethod
    def get_layers_and_weights(_model: object) -> object:
        dict_of_layers = {}
        dict_of_weights = {}
        print(_model.layers)
        for layer in _model.layers:
            print(layer)
            if len(layer.get_weights()) > 0:
                layer_name = layer.name
                weights = layer.get_weights()
                print(layer)
                print(weights)
                if str(layer.__class__.__name__) == "BatchNormalization":
                    print("Visualization of BatchNorm layer is not supported.")
                else:
                    dict_of_layers.update({str(layer_name): {"Name": str(layer.__class__.__name__),
                                                             "Input shape": str(layer.input_shape),
                                                             "Output shape": str(layer.output_shape),
                                                             "Weights shape": str(weights[0].shape)}
                                           })
                    dict_of_weights.update({str(layer_name): weights})

        return dict_of_layers, dict_of_weights

    @staticmethod
    def get_outputs_from_layers(_model, _input_image, _feature_map_dict, _layers_names, _diag=False):
        # getting outputs from every layer and storing it in dictionary
        feature_maps = _model.predict(_input_image)

        for index, singleLayers in enumerate(_layers_names):
            if _diag is True:
                if "diag" in singleLayers:

                    if len(feature_maps[index].shape) == 3:
                        _feature_map_dict.update({str(singleLayers): feature_maps[index]})
                    else:
                        _feature_map_dict.update({str(singleLayers): feature_maps[index][0]})

            else:

                if len(feature_maps[index].shape) == 3:
                    _feature_map_dict.update({str(singleLayers): feature_maps[index]})
                else:
                    _feature_map_dict.update({str(singleLayers): feature_maps[index][0]})

    @staticmethod
    def get_weights_from_model(_dict_of_layers, _dict_of_weights):
        for layer_name in _dict_of_layers:
            weights = _dict_of_layers[layer_name].get_weights()
            _dict_of_weights.update({str(layer_name): weights[0]})
