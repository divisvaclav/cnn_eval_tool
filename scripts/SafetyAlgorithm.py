from scripts.CNNWrapper import CNNWrapper
import scripts.GeneralFunctions as GeneralFunctions

import copy
import collections
import logging
import os
import numpy as np
import json
from tqdm import tqdm
import datetime

import matplotlib.pyplot as plt

plt.rcParams.update({'figure.max_open_warning': 0})


class SafetyAlgorithm(CNNWrapper):

    def __init__(self):
        super().__init__()

        self.fun = GeneralFunctions.GeneralFunctions()

        self.feature_map_dict = {}
        self.max_responses_indices_dict = {}
        self.fm_sum_responses_dict = {}

        self.num_of_max_activations = 5

        # output_layer = "class_output"
        # output = "fc1000"
        self.output_layer = "predictions"

        self.criticality_tau = 0.5

        log_file_dir = os.path.join('data', 'logging')
        if not os.path.exists(log_file_dir):
            os.makedirs(log_file_dir)

        self.logger = logging.getLogger('safety_application')
        self.logger.setLevel(logging.DEBUG)

        today = "_{}".format(datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H%M%S_%f'))
        self.fh = logging.FileHandler(os.path.join(log_file_dir, str(today) + '_SafetyAlgorithm_logger.log'))
        # create formatter and add it to the handlers
        self.formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
        self.fh.setFormatter(self.formatter)
        # add the handlers to the logger
        self.logger.addHandler(self.fh)

        self.statistics_dict = collections.defaultdict(list)

    def clear_variables(self):
        self.feature_map_dict = {}
        self.max_responses_indices_dict = {}
        self.fm_sum_responses_dict = {}

    def calculate_max_responses(self, fmap, _CNA=False):
        self.fm_sum_responses_dict = {}
        self.max_responses_indices_dict = {}
        max_responses_dict = {}
        for layer in fmap.keys():
            activations = []
            fm = fmap[layer]
            if len(fm.shape) > 1:
                if self.dict_of_layers[layer].__class__.__name__ == "Conv2D":
                    n = fm.shape[2]
                    for i in range(n):
                        activations.append(np.sum(np.abs(fm[:, :, i])))
                elif self.dict_of_layers[layer].__class__.__name__ == "DepthwiseConv2D":
                    n = fm.shape[2]
                    for i in range(n):
                        activations.append(np.sum(np.abs(fm[:, :, i])))
            # for DENSE layers
            else:
                n = fm.shape[0]
                for i in range(n):
                    activations.append(np.sum(fm[i]))

            # taking the last X max responses
            if _CNA:
                indexes = np.argsort(np.asarray(activations))
            else:
                ind = np.argsort(np.asarray(activations))
                n = ind.shape[0]
                indexes = ind[n - self.num_of_max_activations:]

            self.Functions.setKey(self.fm_sum_responses_dict, str(layer), activations)

            for max_response_fm_index in indexes:
                self.Functions.setKey(self.max_responses_indices_dict, str(layer), max_response_fm_index)
                if len(fm.shape) > 1:
                    self.Functions.setKey(max_responses_dict, str(layer).lower(), fm[:, :, indexes])
                else:
                    self.Functions.setKey(max_responses_dict, str(layer).lower(), fm[indexes])

    @staticmethod
    def demask_filter_layer(_layers_name, _dict_of_layers, _dict_of_weights):
        if _layers_name not in _dict_of_weights.keys():
            logging.error("Layer not found in loaded model.")
        else:

            if len(_dict_of_weights[_layers_name]) == 2:
                weights = copy.deepcopy(_dict_of_weights[_layers_name][0])
                biases = copy.deepcopy(_dict_of_weights[_layers_name][1])
                _dict_of_layers[_layers_name].set_weights([weights, biases])
            else:
                weights = copy.deepcopy(_dict_of_weights[_layers_name][0])
                _dict_of_layers[_layers_name].set_weights([weights])

    @staticmethod
    def mask_filter_layer(_layers_name, _list_of_masked_filters, _dict_of_layers, _dict_of_weights):
        if _layers_name not in _dict_of_weights.keys():
            logging.error("Layer not found in loaded model.")
        else:

            if len(_dict_of_weights[_layers_name]) == 2:
                weights = copy.deepcopy(_dict_of_weights[_layers_name][0])
                biases = copy.deepcopy(_dict_of_weights[_layers_name][1])

                if len(weights.shape) == 4:  # to filter only depth separable and conv layers

                    if weights.shape[3] == 1:
                        for filters in _list_of_masked_filters:
                            weights[:, :, filters, :] = 0.0
                            # weights[:,:,:,:] = 0.0
                        _dict_of_layers[_layers_name].set_weights([weights, biases])

                    else:
                        if weights.shape[3] < max(_list_of_masked_filters):
                            logging.error("Wrong filter index - layer contains less filters.")
                        else:
                            # weights[:,:,:,_filter_number] = 0.0
                            for filters in _list_of_masked_filters:
                                weights[:, :, :, filters] = 0.0
                                # weights[:,:,:,:] = 0.0
                            _dict_of_layers[_layers_name].set_weights([weights, biases])
                            # else:
                            # _dict_of_layers[_layers_name].set_weights([weights])
                # to filter only dense layers
                elif len(weights.shape) == 2:
                   for neurons in _list_of_masked_filters:
                       weights[:, neurons] = 0.0
                       _dict_of_layers[_layers_name].set_weights([weights, biases])
            else:
                weights = copy.deepcopy(_dict_of_weights[_layers_name][0])

                if len(weights.shape) == 4:  # to filter only depth separable and conv layers

                    if weights.shape[3] == 1:
                        for filters in _list_of_masked_filters:
                            weights[:, :, filters, :] = 0.0
                            # weights[:,:,:,:] = 0.0
                        _dict_of_layers[_layers_name].set_weights([weights])

                    else:
                        if weights.shape[3] < max(_list_of_masked_filters):
                            logging.error("Wrong filter index - layer contains less filters.")
                        else:
                            # weights[:,:,:,_filter_number] = 0.0
                            for filters in _list_of_masked_filters:
                                weights[:, :, :, filters] = 0.0
                                # weights[:,:,:,:] = 0.0
                            _dict_of_layers[_layers_name].set_weights([weights])
                            # else:
                            # _dict_of_layers[_layers_name].set_weights([weights])
                # to filter only dense layers
                elif len(weights.shape) == 2:
                    for neurons in _list_of_masked_filters:
                        weights[:, neurons] = 0.0
                        _dict_of_layers[_layers_name].set_weights([weights])

    @staticmethod
    def calculate_criticality_adversary(des_adv_conf, new_conf, new_ind, ori_ind, adv_ori_conf, criticality_tau):
        # 1 case:
        """ if the new prediction has smaller confidence than original one """
        if new_ind != ori_ind and (des_adv_conf - adv_ori_conf) < criticality_tau:
            criticality = des_adv_conf - adv_ori_conf

        # 2 case:
        elif new_ind == ori_ind:
            if new_conf > 0.5:
                criticality = -2.0  # clip the criticality to 2
            else:
                criticality = -1.0 * (1 / (1 - new_conf))
        # 3 case:
        else:
            # Anti critical neurons, the criticality should be negative
            criticality = des_adv_conf - adv_ori_conf

        return criticality

    @staticmethod
    def calculate_criticality(original_conf, new_conf, original_ind, new_ind, criticality_tau):

        # 1 case:
        """ if the new prediction has smaller confidence than original one """
        if ((original_conf - new_conf) > criticality_tau) and (
                original_ind == new_ind):
            criticality = original_conf - new_conf

        # 2 case:
        elif original_ind != new_ind:
            if new_conf > 0.5:
                criticality = 2.0  # clip the criticality to 2
            else:
                criticality = 1 / (1 - new_conf)

        # 3 case:
        else:
            # Anti critical neurons, the criticality should be negative
            criticality = original_conf - new_conf

        return criticality

    def get_conf_and_class(self, path, file_name, des_cls, remove=False):

        # Get outputs from chosen layers and calculate maximum responses
        output = self.custom_layers_model.predict(self.input_image_np)[-1]

        if len(output.shape) > 1:
            output_layer = output[0]
        else:
            output_layer = output

        pre_ind = np.argmax(output_layer)
        pre_conf = np.max(output_layer)
        pre_cls = self.list_of_classes_labels[pre_ind]
        des_cls_ind = self.list_of_classes_labels.index(des_cls)
        des_conf = output_layer[des_cls_ind]

        if ((des_cls != pre_cls) or (des_cls == pre_cls and pre_conf < 0.5)) and remove is True:
            os.remove(os.path.join(path, file_name))

        self.logger.debug("Prediction index: {}, class: {}, confidence: {}, for image: {}".format(
                            pre_ind, pre_cls, pre_conf, file_name))

        return des_cls_ind, des_cls, des_conf

    def mask_and_get_confidence(self, layer, neuron_index, path, file_name, des_cls, remove=False):
        original_ind, original_cls, original_conf = self.get_conf_and_class(path, file_name, des_cls, remove)

        self.mask_filter_layer(layer, neuron_index, self.dict_of_layers, self.dict_of_weights)
        self.get_outputs_from_layers(self.custom_layers_model, self.input_image_np, self.feature_map_dict,
                                     self.list_of_layers_names, _diag=False)

        # Get outputs from chosen layers and calculate maximum responses // added output
        output = self.custom_layers_model.predict(self.input_image_np)[-1]
        if len(output.shape) > 1:
            output_layer = output[0]
        else:
            output_layer = output

        output = self.custom_layers_model.predict(self.input_image_np)[-1]
        new_ind = np.argmax(output_layer)
        new_conf = output_layer[new_ind]

        criticality = self.calculate_criticality(original_conf, new_conf, original_ind, new_ind,
                                                 self.criticality_tau)

        print(" Layer: {}, Neuron {}, ori class: {}, conf: {:.3f}, new class: {} conf: {:.3f},"
              " criticality {:.3f}".format(
                layer, neuron_index, original_cls, original_conf, output,
                self.list_of_classes_labels[new_ind], new_conf, criticality))

        self.demask_filter_layer(layer, self.dict_of_layers, self.dict_of_weights)

    # criticalDecissionPath
    def analyse_CDP_dependently_masking(self, _path, image_name, _model_name, _number_of_neurons,
                                        original_ind, original_cls, original_conf):

        statistics_dict = collections.defaultdict(list)

        statistics_dict["ORIGINAL"].append({0: original_conf})

        for every_layer in self.list_of_layers_names:

            self.get_outputs_from_layers(self.custom_layers_model, self.input_image_np, self.feature_map_dict,
                                         self.list_of_layers_names, _diag=False)

            self.calculate_max_responses(self.feature_map_dict, _CNA=True)

            max_response_filter_index = self.max_responses_indices_dict[every_layer][:_number_of_neurons]

            # mask the related neuron
            if isinstance(max_response_filter_index,  list):
                self.mask_filter_layer(every_layer, max_response_filter_index, self.dict_of_layers,
                                       self.dict_of_weights)
            else:
                self.mask_filter_layer(every_layer, [max_response_filter_index], self.dict_of_layers,
                                       self.dict_of_weights)
            output = self.custom_layers_model.predict(self.input_image_np)[-1]

            new_ind = np.argmax(output[0])
            new_conf = output[0][new_ind]
            or_conf = output[0][original_ind]

            criticality = self.calculate_criticality(original_conf, new_conf, original_ind, new_ind,
                                                     self.criticality_tau)
            print(criticality)
            index_key = str(max_response_filter_index)
            statistics_dict[every_layer].append({index_key: or_conf})

        for every_layer in self.list_of_layers_names:
            self.demask_filter_layer(every_layer, self.dict_of_layers, self.dict_of_weights)

        self.fun.plot_CDP_dependently_masking_results(_path, statistics_dict, original_conf, original_cls, image_name,
                                                      _model_name, _number_of_neurons)

    def analyse_CDP_plain_masking(self, _path, image_name, _model_name, des_conf, des_ind, des_cls, ori_ind,
                                  adversary=False):

        self.logger.debug(" ----------- Starting the CDPA_plain_masking ----------- ")

        statistics_dict_temp = collections.defaultdict(list)
        statistics_dict_json = collections.defaultdict(list)

        for each_layer_index in range(len(self.list_of_layers_names) - 1):
            each_layer = self.list_of_layers_names[each_layer_index]
            logging.debug("Processing layer: " + each_layer)

            for each_filter in tqdm(range(self.dict_of_weights[each_layer][0].shape[-1])):
                # mask the related neuron
                self.mask_filter_layer(each_layer, [each_filter], self.dict_of_layers, self.dict_of_weights)
                output = self.custom_layers_model.predict(self.input_image_np)[-1]
                if len(output.shape) > 1:
                    output_layer = output[0]
                else:
                    output_layer = output
                new_ind = np.argmax(output_layer)
                new_conf = output_layer[new_ind]

                if adversary:
                    adv_conf = output_layer[new_ind]
                    adv_des_conf = output_layer[ori_ind]
                    criticality = self.calculate_criticality_adversary(des_conf, adv_conf, des_ind, ori_ind,
                                                                       adv_des_conf, self.criticality_tau)
                else:
                    criticality = self.calculate_criticality(des_conf, new_conf, des_ind, new_ind,
                                                             self.criticality_tau)

                statistics_dict_temp[each_layer].append({str(each_filter): criticality})
                statistics_dict_json[each_layer].append({str(each_filter): str(criticality)})

            # have to demask filter only at the end of the layer iteration
            self.demask_filter_layer(each_layer, self.dict_of_layers, self.dict_of_weights)

        self.logger.debug(" ----------- CDPA finished ----------- ")
        self.statistics_dict[image_name].append(statistics_dict_json)
        conv_dict = collections.defaultdict(list)
        proj_dict = collections.defaultdict(list)
        for layers_name in statistics_dict_temp.keys():
            if "project" in layers_name:
                for indices_dict in statistics_dict_temp[layers_name]:
                    proj_dict[layers_name].append(indices_dict)
            else:
                for indices_dict in statistics_dict_temp[layers_name]:
                    conv_dict[layers_name].append(indices_dict)

        self.fun.plot_CDP_results(_path, proj_dict, image_name, _model_name, des_cls,
                                  self.dict_of_weights, self.criticality_tau)

    def analyse_CDP_plain_masking_offline(self, image_name):

        dictionary_path = os.path.join("data", "statistics_dict" + image_name + ".json")
        with open(dictionary_path, 'r') as fp:
            data = json.load(fp)
        print(data)

    def analyse_accuracy_of_masked_model(self, _files_path, _path_benchmark, _models, _classes, worst_neurons_dict,
                                         _n_worst=20):
        temp_worst_neurons = dict()

        for layer in worst_neurons_dict.keys():
            neurons_dicts = worst_neurons_dict[layer]
            for neurons_dict in neurons_dicts:
                for index, criticality in neurons_dict.items():
                    self.fun.setKey(temp_worst_neurons, criticality, [layer, index])

        sorted_worst_neurons = dict(sorted(temp_worst_neurons.items(), reverse=True))
        files = [x for x in os.listdir(_files_path) if not x.startswith('.')]
        files.sort()
        list_of_neurons = list(sorted_worst_neurons.keys())

        list_of_neurons = list_of_neurons[:_n_worst]
        list_of_acc = list()
        list_of_acc_std = list()
        list_of_worst_neurons = list()
        # first original accuracy
        acc, std_acc = self.calculate_accuracy(_files_path, files, _models, _classes)
        list_of_acc.append(acc)
        list_of_acc_std.append(std_acc)
        list_of_worst_neurons.append("No masking")

        for worst_neuron in list_of_neurons:
            layer = sorted_worst_neurons[worst_neuron][0][0]
            neuron_index = sorted_worst_neurons[worst_neuron][0][1]
            list_of_worst_neurons.append(layer + " " + str(neuron_index))
            # print(layer + " neuron: " + str(neuron_index))
            self.mask_filter_layer(layer, [int(neuron_index)], self.dict_of_layers, self.dict_of_weights)
            acc, std_acc = self.calculate_accuracy(_files_path, files, _models, _classes)
            list_of_acc.append(acc)
            list_of_acc_std.append(std_acc)
            self.demask_filter_layer(layer, self.dict_of_layers, self.dict_of_weights)

        fig_dir = os.path.join(_path_benchmark, _models, _classes)
        if not os.path.exists(fig_dir):
            os.makedirs(fig_dir)
        self.fun.plot_histogram_with_err(fig_dir, _models, _classes, _n_worst,
                                         list_of_worst_neurons, list_of_acc, list_of_acc_std)
