# Neural Criticality: Validation of Convolutional Neural Networks

Our open source code should help to understand and rely on the inference processes of Convolution Neural Networks (CNNs).

We provide a code which calculates criticality of each neuron of each layer. Furthermore our code allows you to visualize this criticality and find a weak spots of your network.

## Code structure:
- data - all logs,statistics or figure can be find here
- scripts - all required scripts
- scripts/AdversaryModul.py - takes a model and dataset as input and generated adversary samples via FSGM
- scripts/CNNWrapper.py - wrapper for a chosen CNN, contains getters and setters
- scripts/SafetyAlgorithm.py - inherits CNNWrapper and contains all analysis methos


Currently supported ML Frameworks:
keras - all pretrained and custom models (.h5, .json)

### Analyzing of following models/layers:
Our framework supports classification models, but it will be in the future extended to detection models as well.

For the moment following layers are supported:
- Conv2D
- Dense
- DepthwiseConv2D

## Installation

`python3 -m venv --system-site-packages venv`

`source ./venv/bin/activate`

`pip install --upgrade pip`

`pip install requirements.txt`

## Run a simulation
1. adapt path to your dataset and models in "main_wo_gui.py"
2. setup the parameters of you analysis
3. run analysis

## Analysis results
We implemented several visualization possibilities. All you can find in "data/benchmark"

### Results on original dataset for all models and layers
![EfficientNet B0 layers criticality](./data/images_gitlab/normal/EfficientNetB0_mountain_bike_criticality_result.png)
![MobileNetv2 layers criticality](./data/images_gitlab/normal/MobileNetV2_mountain_bike_criticality_result.png)
![Resnet50V2 layers criticality](./data/images_gitlab/normal/ResNet50V2_mountain_bike_criticality_result.png)
![VGG16 layers criticality](./data/images_gitlab/normal/VGG16_mountain_bike_criticality_result.png)

### Results on original dataset for all models and specific layer
![EfficientNet B0 neurons criticality](./data/images_gitlab/normal/EfficientNetB0_block2a_project_conv_criticality_result.png)
![MobileNetv2 neurons criticality](./data/images_gitlab/normal/MobileNetV2_block_1_project_criticality_result.png)
![Resnet50V2 neurons criticality](./data/images_gitlab/normal/ResNet50V2_conv1_conv_criticality_result.png)
![VGG16 neurons criticality](./data/images_gitlab/normal/VGG16_block1_conv1_criticality_result.png)

### Results on adversary dataset
Accuracy-stabiltiy results of masking the most critical neurons
![MobileNetv2 adversary most critical neurons](./data/images_gitlab/adversary/MobileNetV2_mountain_bike_100_accuracy_result_adversary.png)
![Resnet50V2 adversary most critical neurons](./data/images_gitlab/adversary/ResNet50V2_mountain_bike_100_accuracy_result.png)
![VGG16 adversary most critical neurons](./data/images_gitlab/adversary/VGG16_mountain_bike_100_accuracy_result.png)


### License
No licence