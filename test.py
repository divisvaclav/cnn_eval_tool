import torch
import urllib
from PIL import Image
from torchvision import transforms
import torch.nn as nn
from scripts.PytorchWrapper import PytWrapper
import matplotlib.pyplot as plt
from torchvision import models

py_wrapper = PytWrapper()
py_wrapper.open_alexanet(pretrained=True)
print(py_wrapper.model)

conv_layers, weights = py_wrapper.getConv2D_weights_and_layers()
dict_of_layers, dict_of_weights = py_wrapper.get_weights_and_layers()

print(conv_layers)
print(dict_of_layers)
print("********************************************************")
transform = transforms.Compose([
    transforms.Resize((224, 224)),
    transforms.ToTensor(),
    transforms.Normalize(mean=0., std=1.)
])

image = Image.open(str('dog.jpg'))
plt.imshow(image)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = py_wrapper.model.to(device)

image = transform(image)
print(f"Image shape before: {image.shape}")
image = image.unsqueeze(0)
print(f"Image shape after: {image.shape}")
image = image.to(device)

print(model(image))

outputs = []
names = []
for layer in conv_layers[0:]:
    image = layer(image)
    outputs.append(image)
    names.append(str(layer))
print(len(outputs))

#print feature_maps
for feature_map in outputs:
    print(feature_map.shape)

new_model = nn.Sequential()
layer = conv_layers[0]
new_model.add_module(layer.__class__.__name__, layer)
print(new_model)
layer.weight[0]
print(layer.weight.shape[3] != 1)

print(py_wrapper.get_layers_split(how_many_outputs=2))